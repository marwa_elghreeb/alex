<?php
$age = date_diff(date_create("1989-11-25"), date_create('now'))->y;

var_dump($age);

date_default_timezone_set('UTC');
$time = time();

echo date('l dS F Y h:i:s A', $time);
echo '<br /><br />';

require('I18N/Arabic.php');
$Ar = new I18N_Arabic('Date');

$fix = $Ar->dateCorrection ($time);

echo $Ar->date('l dS F Y h:i:s A',
    $time, $fix);
echo '<br /><br />';

$day = $Ar->date('j', $time, $fix);
$dir = 'I18N/Arabic/Examples/';
echo '<a href="'.$dir.'Moon.php?day='.$day;
echo '" class="ar_link" target=_blank';
echo '>القمر الليلة</a>';
echo '<br /><br />';

$Ar->setMode(2);
echo $Ar->date('l dS F Y h:i:s A', $time);
echo '<br /><br />';

$Ar->setMode(3);
echo $Ar->date('l dS F Y h:i:s A', $time);
echo '<br /><br />';

$Ar->setMode(4);
echo $Ar->date('l dS F Y h:i:s A', $time);
echo '<br /><br />';

$Ar->setMode(6);
echo $Ar->date('l dS F Y h:i:s A', $time);
?> 