<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamesDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exames_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('competitionsId');
            $table->integer('levelId');
            $table->date('day');
            $table->text('from');
            $table->text('to');
            $table->text('limit');
            $table->text('type');
            $table->text('period');
            $table->text('break');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exames_dates');
    }
}
