<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamesHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exames_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('examId');
            $table->integer('userId');
            $table->date('day');
            $table->text('from');
            $table->text('to');
            $table->text('type');
            $table->text('state');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exames_hours');
    }
}
