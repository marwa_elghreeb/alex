<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitors', function (Blueprint $table) {
            $table->increments('id');
            $table->increments('userId');
            $table->text('code');
            $table->text('userName');
            $table->text('nationalId');
            $table->text('government');
            $table->text('city');
            $table->text('area');
            $table->text('address');
            $table->text('phone1');
            $table->text('phone2');
            $table->text('phoneHome');
            $table->text('job');
            $table->text('gender');
            $table->text('age');
            $table->text('email');
            $table->text('education');
            $table->text('shareBefore');
            $table->text('imageName');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitors');
    }
}
