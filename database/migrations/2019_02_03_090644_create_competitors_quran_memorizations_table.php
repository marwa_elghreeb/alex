<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompetitorsQuranMemorizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('competitors_quran_memorizations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userId');
            $table->integer('teatcherId');
            $table->integer('code');
            $table->integer('jobCode');
            $table->text('job');
            $table->text('relatedTo');
            $table->text('level');
            $table->text('readType');
            $table->text('images');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('competitors_quran_memorizations');
    }
}
