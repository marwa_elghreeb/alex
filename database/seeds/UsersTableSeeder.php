<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = \App\User::create([
            'serialNo' => '1',
            'name' => 'super_admin',
            'email' => 'super_admin@admin.com',
            'password' => bcrypt('123456'),
        ]);
    }
}
