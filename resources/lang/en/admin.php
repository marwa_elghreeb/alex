<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Change Language' => 'Change Language',
    'Arabic' => 'Arabic',
    'English' => 'English',
    'Logout' => 'Logout',
    'Home' => 'Home',
    'User & Groups' => 'User & Groups',
    'Menu' => 'Menu',
    'User groups' => 'User groups',
    'Users' => 'Users',
    //
    'Home' => 'Home',
    'Control panel' => 'Control panel',
    'Welcom to your dashboard' => 'Welcom to your dashboard',
    'Patient No' => 'Patient No',
    'Reservation No' => 'Reservation No',
    'Visitors No' => 'Visitors No',
    'Applay by website' => 'Applay by website',
    'Show Data' => 'Show Data',
    'Add new item' => 'Add new item',
    'Name' => 'Name',
    'Edit' => 'Edit',
    'Delete' => 'Delete',
    'Permission' => 'Permission',
    'Save' => 'Save',
    'Cancel' => 'Cancel',
    'Edit item' => 'Edit item',
    'Group name' => 'Group name',
    'Email' => 'Email',
    'Serial No' => 'Serial No',
    'Password' => 'Password',
    'Phone' => 'Phone',
    'Symbole' => 'Symbole',
    'Image' => 'Image',
    'Logo' => 'Logo',
    'Site Name' => 'Site Name',
    'Address' => 'Address',
    'Social media setting' => 'Social media setting',
    'Icon' => 'Icon',
    'Link' => 'Link',
    'Tittel' => 'Title',
    'Descrption' => 'Description',
    'Position' => 'Position',
    //
    'General Competitions' => 'General Competitions',
    'private Competitions' => 'private Competitions',
    'Quran memorization' => 'Quran memorization',
    'Contestants' => 'Contestants',
    'Volunteers' => 'Volunteers',
    'Sons of workers' => 'Sons of workers',
    'Competitions Type' => 'Competitions Type',
    'OrderNo' => 'Order No',
    'Parts' => 'Parts',



];
