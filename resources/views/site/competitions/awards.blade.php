@extends('site.web.app')
@section('content')

    <section class="competitionTerms-section py-5">
        <div class="container-fluid">
            <div class="container">
                <div class="row mb-5">
                    <div class="navigationBar">
                        <a class="primary-font" href="{{ action('Site\IndexController@index') }}"> الرئيسيه</a>
                        <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                        <span class="secondary-font"> {{ $catName }}</span>
                        <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                        <span class="secondary-font">شروط المسابقة </span>
                    </div>
                </div>


                <?php $checkData = \App\UserCompetitions::where(['userId' => session('userId') ])->first(); ?>

                @if(count($checkData) > 0)
                    <div class="alert alert-info">
                        تم التسجيل  فى المسابقه وسيتم تحديد الاختبار فى اقرب وقت
                    </div>
                @endif

                <div class="alert alert-info" style="display: none" id="msgDiv">
                   <span id="msgSpan"></span>
                </div>


                <div class="row mb-3">

                    @foreach($levels as $data)
                    <div class="col-md-4">
                        <div class="competitionAwards">
                            <div class="competitionAwards-header p-3">
                                <ul class="list-unstyled mb-0">
                                    <li>
                                        <i class="far fa-hand-point-left"></i>
                                        <span class="fontsize-18 red-font"> {{ $data->name }} </span>
                                    </li>
                                </ul>
                            </div>
                            <div class="competitionAwards-body">
                                <ul class="list-unstyled">


                                    @foreach($data->awards as $award)
                                    <li class="text-center p-2">
                                        <span> {{ $award->name }} :  {{ $award->title }} جنيه مصري</span>

                                    </li>
                                        @endforeach


                                </ul>

                                @if(count($checkData) > 0)
                                    @else


                                    <form  action="#" method="post">
                                        {{ csrf_field() }}

                                        <div class="form-body">
                                            <div class="mb-4">
                                                <label data-error="wrong" data-success="right" for="modalLRInput10"> الاجزاء من </label>

                                                    <select class="form-control">
                                                        @for($i = 1 ; $i <= 30 ; $i++)
                                                        <option>{{ $i }}</option>
                                                            @endfor
                                                    </select>


                                            </div>
                                            <div class="mb-4">
                                                <label data-error="wrong" data-success="right" for="modalLRInput11"> الاجزاء من </label>
                                                <select class="form-control">
                                                    @for($i = 1 ; $i <= 30 ; $i++)
                                                        <option>{{ $i }}</option>
                                                    @endfor
                                                </select>
                                            </div>

                                            <div class="mb-4">
                                                <label data-error="wrong" data-success="right" for="modalLRInput11">  القراءات </label>
                                                <select class="form-control">
                                                    @foreach( $data->readers as $reader)
                                                        <option> {{ $reader->name }} </option>
                                                        @endforeach

                                                </select>
                                            </div>


                                            @if($catData->id ==2)

                                                <div class="mb-4">
                                                    <label data-error="wrong" data-success="right" for="modalLRInput11">  صله القرابه </label>
                                                    <select class="form-control">
                                                        @foreach( $relations as $rel)
                                                            <option> {{ $rel->name }} </option>
                                                        @endforeach

                                                    </select>
                                                </div>

                                            @endif

                                        </div>



                                <div class="text-center">
                                    <a href="#" class="btn readMore-sm-btn mb-3 applayComp" role="button"
                                    data-userId="{{session('userId')}}" data-url="{{ action('Site\UserController@checkUserIsValid') }}"
                                    data-compId="{{ $data->typeId }}" data-levelId="{{  $data->id  }}">الاشتراك </a>
                                </div>
                                    </form>
                                    @endif

                            </div>
                        </div>
                    </div>
                        @endforeach


                </div>




            </div>
        </div>
    </section>


@endsection