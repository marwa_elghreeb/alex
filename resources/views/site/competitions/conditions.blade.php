@extends('site.web.app')
@section('content')



    <section class="competitionTerms-section py-5">
        <div class="container-fluid">
            <div class="container">
                <div class="row mb-5">
                    <div class="navigationBar">
                        <a class="primary-font" href="{{ action('Site\IndexController@index') }}"> الرئيسيه</a>
                        <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                        <span class="secondary-font"> {{ $catName }}</span>
                        <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                        <span class="secondary-font">شروط المسابقة </span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="competition-terms">
                            <div class="text-center mb-5">
                                <h5 class="fontsize-22 primary-font">شروط المسابقة </h5>
                            </div>
                            <div class="general-terms">
                                <ul class="list-unstyled">


                                    @foreach($conditions as $cond)
                                    <li class="mb-3">
                                        <p class="mb-0"><i class="far fa-hand-point-left mr-2"></i><span
                                                    class="red-font fontweight-9"> {{ $cond->name }} : </span>

                                            {{ $cond->title }}
                                        </p>

                                    </li>
                                        @endforeach


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection