@extends('site.web.app')
@section('content')

    <section class="quran-section py-5">
        <div class="container-fluid">
            <div class="container">
                <div class="row mb-5">
                    <div class="navigationBar">
                        <a class="primary-font" href="{{ action('Site\IndexController@index') }}"> الرئيسيه</a>
                        <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                        <span class="secondary-font">{{ $catName }}</span>
                    </div>
                </div>


                <div class="row list-title red-color p-3 mb-3">
                    <div class="col-md-3 text-center">
                        <span class="fontsize-15 white-font">المسابقات</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <span class="fontsize-15 white-font">التاريخ من</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <span class="fontsize-15 white-font">التاريخ الى</span>
                    </div>

                    <div class="col-md-3 text-center">
                        <span class="fontsize-15 white-font"></span>
                    </div>
                </div>


                @foreach($allComp as $com)
                <div class="row list-items p-3 mb-3">
                    <div class="col-md-3 text-center">
                        <span class="fontsize-15 primary-font">{{ $com->name }}</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <span class="fontsize-15 primary-font">{{ $com->dateFrom }}</span>
                    </div>
                    <div class="col-md-3 text-center">
                        <span class="fontsize-15 primary-font">{{ $com->dateTo }}</span>
                    </div>


                    <div class="col-md-3 text-center">

                        @if( time() <= strtotime($com->dateTo))
                        <a href="{{ action('Site\CompetitionsController@conditions' , $com->catId) }}" class="btn btn-info" role="button" >الشروط </a>
                        <a href="{{ action('Site\CompetitionsController@bands' , $com->catId) }}" class="btn btn-info" role="button" >البنود </a>
                        <a href="{{ action('Site\CompetitionsController@awards' , $com->catId) }}" class="btn btn-info levels" role="button" >المستويات </a>
                        <!--a href="#" class="btn btn-info" role="button" >الاشتراك </a-->
                            @else

                            تم الانتهاء من المسابقه
                            @endif



                    </div>

                </div>
                    @endforeach



            </div>


        </div>
        </div>
    </section>


@endsection