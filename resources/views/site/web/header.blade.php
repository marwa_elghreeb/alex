<!DOCTYPE html>
<html lang="en" dir="rtl">
<head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> {{ $setting->siteName }}</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.rtlcss.com/bootstrap/v4.0.0/css/bootstrap.min.css"
          integrity="sha384-P4uhUIGk/q1gaD/NdgkBIl3a6QywJjlsFJFk7SPRdruoGddvRVSwv5qFnvZ73cpz" crossorigin="anonymous">
    <link rel="stylesheet" href="{{ URL ::to ('assets/site/css/main.css')}}">
    <link rel="stylesheet" type="text/css" href="//www.fontstatic.com/f=sky,sky-bold,flat-jooza" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="icon" href="{{ URL ::to ('public/images/favicon.png')}}" type="image/x-icon" />
</head>
<body>


<body>
<header>
    <div class="top-block"></div>
    <div class="container-fluid header-section">
        <div class="container">
            <div class="row d-flex flex-row-reverse">
                <div class="date-block mr-3">
                    <span class="white-font fontsize-13 mx-2">
                        <?php
                        date_default_timezone_set('UTC');
                        $time = time();
                        require('I18N/Arabic.php');
                        $Ar = new I18N_Arabic('Date');
                        $fix = $Ar->dateCorrection ($time);
                        echo $Ar->date('l dS - F - Y ',$time, $fix);
                        ?> </span>
                    <span class="primary-font">|</span>
                    <span class="white-font fontsize-13 mx-2">
                        <?php $date = \App\Helpers\Helper::ArabicDate();
                    echo $date;?>  </span>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="header-logo mb-3">
                        <img src="{{ URL ::to ('public/images/'.$setting->logo)}}" alt="logo">
                    </div>
                </div>
                <div class="col-md-6">


                    @if(empty(session('userId')))
                    <div class="my-md-5 my-sm-3">
                        <ul class="nav sign-block justify-content-md-end">
                            <div class="btn sign-btn ml-5">
                                <a  href="{{ action('Site\UserController@login') }}" role="button" aria-haspopup="true" aria-expanded="true">

                                دخول / تسجيل الدخول

                                </a>
                            </div>
                        </ul>

                   </div>

                    <!--  After login-->
                    @else
                        <?php $userData = \App\UserWeb::find(session('userId')); ?>

                    <div class="signed my-md-5 my-sm-3">
                        <ul class="nav justify-content-md-end">
                            <li class="nav-item dropdown show ml-3 current-user-nav">
                                <a class="avatar-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="true">
                                    <img src="{{ URL ::to ('assets/site/img/img-default.png')}}" alt="Avatar" class="avatar">
                                    <span class="mx-2 primary-font fontsize-16">مرحبا,{{ $userData->name }} </span>
                                </a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="{{ action('Site\UserController@profile') }}">الحساب الشخصي</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="{{ action('Site\UserController@userLogout') }}">تسجيل الخروج</a>
                                </div>
                            </li>
                        </ul>
                    </div>

                        @endif


                </div>
            </div>
        </div>
    </div>
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="#"></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon">
            <i class="fas fa-bars fontsize-29"></i>
          </span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav">


                <li class="nav-item mx-3">
                    <a class="nav-link" href="{{ action('Site\IndexController@index') }}"> الرئيسية</a>
                </li>

                @foreach($category as $cat)
                    <?php $nameArr = json_decode($cat->name , true); ?>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="{{ action('Site\CompetitionsController@index' ,$cat->id) }}">  {{ $nameArr[Lang::getLocale()] }}</a>
                </li>
            @endforeach


                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">تحفيظ القرآن</a>
                </li>
                <li class="nav-item dropdown mx-3">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">قناة المسجد</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="#">المكتبة المرئية</a>
                        <a class="dropdown-item" href="#">المكتبة الصوتية</a>
                        <a class="dropdown-item" href="#">بث مباشر</a>
                    </div>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">الفتاوى والأحكام</a>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">المتطوعين</a>
                </li>
                <li class="nav-item mx-3">
                    <a class="nav-link" href="#">اتصل بنا</a>
                </li>
            </ul>
        </div>
    </nav>
</header>