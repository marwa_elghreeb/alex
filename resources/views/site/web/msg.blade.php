@extends('site.web.app')
@section('content')



    <section class="competitionTerms-section py-5">
        <div class="container-fluid">
            <div class="container">
                <div class="row mb-5">
                    <div class="navigationBar">
                        <a class="primary-font" href="{{ action('Site\IndexController@index') }}"> الرئيسيه</a>
                        <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                        <span class="secondary-font"> تنبيه </span>
                    </div>
                </div>



                <div class="row mb-3">

                    <div class="alert alert-info">
                        تم تسجيل بيانتك بنجاح وسيتم تحديد موعد للاختبار فى اقرب وقت بعد الانتهاء من التسجيل
                    </div>



                </div>




            </div>
        </div>
    </section>


@endsection