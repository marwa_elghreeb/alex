<footer class="pt-5">
    <div class="container-fluid footer-block">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="white-logo mb-3">
                        <img src="{{ URL ::to ('public/images/logo-white.png')}}" alt="logo">
                    </div>
                </div>
                <div class="col-md-4">
                    <h4 class="footer-links-titile fontsize-16 white-font mb-3">روابط مهمة</h4>
                    <ul class="footer-links list-unstyled">
                        <li><a href="#" class="white-font"><i class="fas fa-chevron-left fontsize-14 mr-2"></i>من نحن</a></li>
                        <li><a href="#" class="white-font"><i class="fas fa-chevron-left fontsize-14 mr-2"></i>اتصل بنا</a></li>
                        <li><a href="#" class="white-font"><i class="fas fa-chevron-left fontsize-14 mr-2"></i>شروط الإستخدام</a></li>
                        <li><a href="#" class="white-font"><i class="fas fa-chevron-left fontsize-14 mr-2"></i>سياسة الخصوصية</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="social-links">
                        <h4 class="fontsize-16 white-font mb-3">تابعنا على</h4>
                        <ul class="list-inline">

                            @foreach($social as $seo)
                            <li class="list-inline-item">
                                <a href="{{$seo->link}}" target="_blank"><img src="{{ URL ::to ('assets/site/'.$seo->icon)}}" alt="{{$seo->name}}"></a>
                            </li>
                                @endforeach


                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr class="footer-line">
    <div class="container-fluid">
        <div class="container copyrights-block">
            <div class="row">
                <div class="col-md-6 col-xs-12 text-center-xs">
                    <p class="white-font">جميع الحقوق محفوظة 2019</p>
                </div>
                <div class="col-md-6 col-xs-12 text-center-xs">
                    <p class="text-right white-font">تصميم و تطوير &nbsp;
                        <a id="accura-link" target="_blank" href="http://accuragroup-eg.com/">اكيورا جروب</a></p>
                </div>
            </div>
        </div>
    </div>
    </div>
</footer>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="{{ URL ::to ('assets/site/js/main.js')}}"></script>
<script src="{{ URL ::to ('assets/site/custom.js')}}"></script>



</body>
</html>