@extends('site.web.app')
@section('content')


    <section class="slider-section">
        <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li class="sr-only" data-target="#carouselExampleFade" data-slide-to="0" class="active"></li>
                <li class="sr-only" data-target="#carouselExampleFade" data-slide-to="1"></li>
                <li class="sr-only" data-target="#carouselExampleFade" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">

                @foreach($sliders as $slider)
                <div class="carousel-item @if($slider->id == 1) active @endif">
                    <img class="d-block w-100" src="{{ URL ::to ('public/images/'.$slider->imageName)}}" alt="First slide">
                </div>
                    @endforeach




            </div>
            <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
                <span aria-hidden="true"><i class="fas fa-angle-double-right fontsize-26 white-font"></i></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
                <span aria-hidden="true"><i class="fas fa-angle-double-left fontsize-26 white-font"></i></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>

    <!--===================================================-->
    <section class=" about-us">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class=" fontsize-20">نبذة عن الموقع</h3>
                        <div class="line">
                            <img src="{{ URL ::to ('assets/site/img/line.png')}}" alt="line">
                        </div>
                        <div class="content my-5">
                            <p class="fontsize-16 secondary-font">هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة، لقد تم توليد هذا النص من مولد النص العربى، حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.
                                إذا كنت تحتاج إلى عدد أكبر من الفقرات يتيح لك مولد النص العربى زيادة عدد الفقرات كما تريد، النص لن يبدو مقسما ولا يحوي أخطاء لغوية، مولد النص العربى مفيد لمصممي المواقع على وجه الخصوص، حيث يحتاج العميل فى كثير من الأحيان أن يطلع على صورة حقيقية لتصميم الموقع.</p>
                        </div>
                        <a href="#" class="btn readMore-btn">إقرأ المزيد</a>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-6">
                                <img src="{{ URL ::to ('assets/site/img/about1.png')}}" alt="">
                            </div>
                            <div class="col-md-6">
                                <img src="{{ URL ::to ('assets/site/img/about2.png')}}" alt="">
                                <img class="mt-3" src="{{ URL ::to ('assets/site/img/about3.png')}}" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--===================================================-->
    <section class="news-section">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class=" fontsize-20">جديد أخبار الموقع</h3>
                        <div class="line">
                            <img src="{{ URL ::to ('assets/site/img/line.png')}}" alt="line">
                        </div>
                    </div>
                </div>
                <div class="news mt-5">
                    <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
                        <div class="carousel-inner" role="listbox">



                            @foreach($news as $new)
                            <div class="carousel-item mb-5 @if($new->id == 1) active @endif">
                                <div class="slider-title mb-3">
                                    <h5 class="fontsize-20 red-font">{{ $new->name }}</h5>
                                </div>
                                <div class="slider-body">
                                    <p class="fontsize-15">
                                      <?php echo strip_tags($new->content); ?>
                                        <span><a class="fontsize-13" href="">المزيد<i class="fas fa-angle-double-left"></i></a></span>
                                    </p>
                                </div>
                            </div>
                                @endforeach



                        </div>
                        <ol class="carousel-indicators">
                            <i class="fas fa-angle-right fontsize-20 red-font" data-target="#carousel-example-1z" data-slide-to="0" class="active"></i>
                            <span class="mx-3"></span>
                            <i class="fas fa-angle-left fontsize-20 red-font" data-target="#carousel-example-1z" data-slide-to="1"></i>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--===================================================-->
    <!--section class="comptitions">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <section id="tabs" class="project-tab">
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-12">
                                        <nav class="mb-5">
                                            <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                                                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">المسابقات العامة</a>
                                                <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">مسابقات أبناء العاملين</a>
                                                <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">مسابقة تحفيظ القرآن</a>
                                            </div>
                                        </nav>
                                        <div class="tab-content" id="nav-tabContent">
                                            <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="wrapper">
                                                                <div class="box a">
                                                                    <img class="d-block w-100" src="img/11.jpg" alt="">
                                                                </div>
                                                                <div class="box b">
                                                                    <img class="d-block w-100" src="img/11.jpg" alt="">
                                                                </div>
                                                                <div class="box c">
                                                                    <img class="d-block w-100" src="img/11.jpg" alt="">
                                                                </div>
                                                                <div class="box d">
                                                                    <img class="d-block w-100" src="img/11.jpg" alt="">
                                                                </div>
                                                                <div class="box e">
                                                                    <img class="d-block w-100" src="img/11.jpg" alt="">
                                                                </div>
                                                                <div class="box f">
                                                                    <img class="d-block w-100" src="img/11.jpg" alt="">
                                                                </div>
                                                                <div class="box g">
                                                                    <img class="d-block w-100" src="img/11.jpg" alt="">
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

                                            </div>
                                            <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
    </section>
    <!--===================================================-->
    <section class="advisory-section">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="fontsize-20">جديد فتاوى الموقع</h3>
                        <div class="line">
                            <img src="{{ URL ::to ('assets/site/img/line.png')}}" alt="line">
                        </div>
                    </div>
                </div>
                <div class="advisory mt-3">
                    <ul class="nav">



                        @foreach($fatwa as $fat)
                        <li class="nav-item">
                            <a class="nav-link" href="#">
                                <i class="far fa-hand-point-left mr-2"></i>{{ $fat->question }}
                            </a>
                        </li>
                            @endforeach


                    </ul>
                </div>
            </div>
        </div>
    </section>
    <section class="prev-comptitions">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class=" fontsize-20">صور المسابقات السابقة</h3>
                        <div class="line">
                            <img src="{{ URL ::to ('assets/site/img/line.png')}}" alt="line">
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-4">
                        <img class="prev-img" src="{{ URL ::to ('assets/site/img/img1.png')}}" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="prev-img" src="{{ URL ::to ('assets/site/img/img2.png')}}" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="prev-img" src="{{ URL ::to ('assets/site/img/img3.png')}}" alt="">
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-4">
                        <img class="prev-img" src="{{ URL ::to ('assets/site/img/img3.png')}}" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="prev-img" src="{{ URL ::to ('assets/site/img/img2.png')}}" alt="">
                    </div>
                    <div class="col-md-4">
                        <img class="prev-img" src="{{ URL ::to ('assets/site/img/img1.png')}}" alt="">
                    </div>
                </div>
                <div class="text-center mt-5">
                    <a href="#" class="btn readMore-btn">المزيد</a>
                </div>
            </div>
        </div>
    </section>
    <section class="contact-section">
        <div class="container-fluid">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class=" fontsize-20">للتواصل مع الإدارة</h3>
                        <div class="line">
                            <img src="{{ URL ::to ('assets/site/img/line.png')}}" alt="line">
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col-md-12">
                        <form action="" method="" id="form-contactus" class="ContactUs-form">
                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="contact_name" class="sr-only">الإسم</label>
                                    <input type="text" class="form-control form-control-lg" id="contact_name" placeholder="الإسم">
                                </div>
                                <div class="form-group col-6">
                                    <label for="contact_email" class="sr-only">البريد الإلكتروني</label>
                                    <input type="email" class="form-control form-control-lg" id="contact_email" placeholder="البريد الإلكتروني">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-6">
                                    <label for="contact_title" class="sr-only">عنوان الرسالة</label>
                                    <input type="text" class="form-control form-control-lg" id="contact_title" placeholder="عنوان الرسالة">
                                </div>
                                <div class="form-group col-6">
                                    <label for="contact_type" class="sr-only">سبب الإتصال</label>
                                    <select class="form-control form-control-lg" id="contact_type">
                                        <option value="">سبب الإتصال</option>
                                        <option value="">أقتراح</option>
                                        <option value="">شكوى</option>
                                        <option value="">اخرى</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="contact_message" class="sr-only">محتوى الرسالة</label>
                                <textarea class="form-control" id="contact_message" rows="3">محتوى الرسالة</textarea>
                            </div>
                            <div class="text-center mt-5">
                                <button type="submit" class="btn readMore-btn">إرسال</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection