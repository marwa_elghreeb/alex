@extends('site.web.app')
@section('content')


    <section class="sign-section py-5">
        <div class="container-fluid">
            <div class="container">

                <div class="row">
                    <div class="col-lg-5 col-md-7 col-sm-10 m-auto">
                        <div class="d-flex justify-content-center">
                            <ul class="nav nav-tabs md-tabs tabs-2 red-color" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link white-font active" data-toggle="tab" href="#panel7" role="tab"><i class="fas fa-user mr-2"></i>تسجيل دخول</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link white-font " data-toggle="tab" href="#panel8" role="tab"><i class="fas fa-user-plus mr-2"></i>إنشاء حساب</a>
                                </li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="panel7" role="tabpanel">
                                <form action="{{ action('Site\UserController@signForm') }}" method="post">
                                    {{ csrf_field() }}

                                    <div class="form-body">
                                        <div class="mb-4">
                                            <label data-error="wrong" data-success="right" for="modalLRInput10">البريد الإلكتروني</label>
                                            <input type="email" name="email" id="modalLRInput10" class="form-control">
                                            @if ($errors->has('email'))
                                                <span class="help-block"> <strong style="color: red;">{{ $errors->first('email') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="mb-4">
                                            <label data-error="wrong" data-success="right" for="modalLRInput11">كلمة المرور</label>
                                            <input type="password" name="password" id="modalLRInput11" class="form-control">
                                            @if ($errors->has('password'))
                                                <span class="help-block "> <strong style="color: red;">{{ $errors->first('password') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="text-center">
                                            <button type="submit" class="btn readMore-btn">تسجيل دخول<i class="fas fa-sign-in ml-1"></i></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane fade in " id="panel8" role="tabpanel">
                                <form role="form" enctype="multipart/form-data" action="{{ action('Site\UserController@signUpForm') }}" method="post">
                                    {{ csrf_field() }}

                                    <div class="form-body">
                                        <div class="mb-4">
                                            <label data-error="wrong" data-success="right" for="modalLRInput11">الإسم</label>
                                            <input type="text" name="name" value="{{ old('name') }}" id="modalLRInput11" class="form-control">
                                            @if ($errors->has('name'))
                                                <span class="help-block "> <strong style="color: red;">{{ $errors->first('name') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="mb-4">
                                            <label data-error="wrong" data-success="right" for="modalLRInput11">رقم الموبايل</label>
                                            <input type="text" name="phone" value="{{ old('phone') }}" id="modalLRInput11" class="form-control">
                                            @if ($errors->has('phone'))
                                                <span class="help-block "> <strong style="color: red;">{{ $errors->first('phone') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="mb-4">
                                            <label data-error="wrong" data-success="right" for="modalLRInput10">البريد الإلكتروني</label>
                                            <input type="email" name="email" value="{{ old('email') }}" id="modalLRInput10" class="form-control">
                                            @if ($errors->has('email'))
                                                <span class="help-block"> <strong style="color: red;">{{ $errors->first('email') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="mb-4">
                                            <label data-error="wrong" data-success="right" for="modalLRInput11">كلمة المرور</label>
                                            <input type="password" name="password" id="modalLRInput11" class="form-control">
                                            @if ($errors->has('password'))
                                                <span class="help-block "> <strong style="color: red;">{{ $errors->first('password') }}</strong></span>
                                            @endif
                                        </div>
                                        <div class="mb-4">
                                            <label data-error="wrong" data-success="right" for="modalLRInput11">إعادة كلمة المرور</label>
                                            <input type="password" name="password_confirmation" id="modalLRInput11"  class="form-control">
                                        </div>

                                        <div class="text-center">
                                            <button type="submit" class="btn readMore-btn">
                                                إنشاء حساب<i class="fas fa-sign-in ml-1"></i></button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




@endsection