@extends('site.web.app')
@section('content')


    <section class="compitition-section py-5">
        <div class="container-fluid">
            <div class="container">
                <div class="row mb-5">
                    <div class="navigationBar">
                        <a class="primary-font" href="{{ action('Site\IndexController@index') }}"> الرئيسيه</a>
                        <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                        <span class="secondary-font">التسجيل في المسابقة</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="compitition-form">
                            <form class="form-horizontal" enctype="multipart/form-data" action="{{ action('Site\UserController@applayCompetitionsForm') }}" method="post">
                                {{ csrf_field() }}

                                <input type="hidden"  name="levelId"  value="{{ $levelId }}">


                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">الاسم </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="name"  value="{{ $userData->name }}">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">الكود</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="code" disabled  value="{{ count($code) + 1 }}">
                                       
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">الرقم القومى</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="nationalId"  value="{{ old('nationalId') }}">
                                        @if ($errors->has('nationalId'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('nationalId') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">المدينه</label>
                                    <div class="col-sm-10">
                                        <select name="city" class="form-control form-control-lg" id="exampleFormControlSelect1">

                                            @foreach($city as $cc)
                                            <option value="{{ $cc->name_ar }}"> {{ $cc->name_ar }} </option>
                                                @endforeach

                                        </select>


                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">المنطقه</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="area"  value="{{ old('area') }}">
                                        @if ($errors->has('area'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('area') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">العنوان</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="address"  value="{{ old('address') }}">
                                        @if ($errors->has('address'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('address') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">رقم الهاتف (1)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone1"  value="{{ $userData->phone }}">
                                        @if ($errors->has('phone1'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('phone1') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">رقم الهاتف(2)</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phone2"  value="{{ old('phone2') }}">
                                        @if ($errors->has('phone2'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('phone2') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">رقم المنزل</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="phoneHome"  value="{{ old('phoneHome') }}">
                                        @if ($errors->has('phoneHome'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('phoneHome') }}</strong></span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">الوظيفه</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="job"  value="{{ old('job') }}">
                                        @if ($errors->has('job'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('job') }}</strong></span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">النوع</label>
                                    <div class="col-sm-10">
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="gender" id="" value="ذكر">
                                            <label class="form-check-label" for="inlineRadio1">ذكر</label>
                                        </div>
                                        <div class="form-check form-check-inline">
                                            <input class="form-check-input" type="radio" name="gender" id="" value="انثى">
                                            <label class="form-check-label" for="inlineRadio1">انثى</label>
                                        </div>
                                        
                                          @if ($errors->has('gender'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('gender') }}</strong></span>
                                        @endif
                                    </div>
                                   
                                </div>


                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">العمر</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="age"  value="{{ old('age') }}">
                                        @if ($errors->has('age'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('age') }}</strong></span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">البريد الالكترونى</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="email" readonly value="{{ $userData->email }}">
                                        @if ($errors->has('email'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('email') }}</strong></span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">المؤهل الدراسى  </label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="education"  value="{{ old('education') }}">
                                        @if ($errors->has('education'))
                                            <span class="help-block"> <strong style="color: red;">{{ $errors->first('education') }}</strong></span>
                                        @endif
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">هل تم المشاركه من قبل</label>
                                    <div class="col-sm-10">
                                        <div class="col-sm-10">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="shareBefore" id="" value="نعم">
                                                <label class="form-check-label" for="inlineRadio1">نعم</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="shareBefore" id="" value="لا">
                                                <label class="form-check-label" for="inlineRadio1">لا</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">الصوره الشخصيه</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="imageName">
                                    </div>
                                </div>





                                <div class="text-center mt-4">
                                    <button type="submit" class="btn readMore-btn">إشتراك</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
    </section>





@endsection