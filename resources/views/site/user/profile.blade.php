@extends('site.web.app')
@section('content')


    <section class="profile-section py-5">
        <div class="container-fluid">
            <div class="container">
                <div class="row mb-5">
                    <div class="navigationBar">
                        <a class="primary-font" href="{{ action('Site\IndexController@index') }}"> الرئيسيه</a>
                        <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                        <span class="primary-font">الحساب الشخصي</span>
                    </div>
                </div>
                <div class="row my-5">
                    <div class="col-lg-3 col-md-4 col-sm-5">
                        <div class="list-group" id="myList" role="tablist">
                            <div class="list-group-item list-group-item-action text-center active" data-toggle="list"
                                 href="#user-img" role="tab">

                                @if(empty($details->imageName))
                                <img class="user-img" src="{{ URL ::to ('assets/site/img/img-default.png')}}" alt="">
                                @else
                                    <img class="user-img" src="{{ URL ::to ('public/images/'.$details->imageName) }}" alt="">
                                    @endif

                                <h5 class="fontsize-14 mt-2">  {{ $userData->name }}</h5>
                            </div>



                            <a class="list-group-item list-group-item-action"
                               data-toggle="list" href="#exams" role="tab"> مواعيد الاختبار</a>



                            <a class="list-group-item list-group-item-action"
                               data-toggle="list" href="#notification" role="tab"> الاشعارات</a>



                        </div>
                    </div>
                    <div class="col-lg-9 col-md-8 col-sm-7">
                        <div class="tab-content">

                            <div class="tab-pane active" id="user-img" role="tabpanel">
                                <div class="p-4">
                                    <div class="name mb-3">
                                        <span class="fontsize-16 red-font fontweight-9">الإسم :</span>
                                        <span class="fontsize-16 secondary-font">{{ $userData->name }}</span>
                                        <a href="#">
                                            <span class="fontsize-22 red-font float-right"><i
                                                        class="fas fa-user-edit"></i></span>
                                        </a>
                                    </div>
                                    <div class="mobile mb-3">
                                        <span class="fontsize-16 red-font fontweight-9">رقم الموبايل :</span>
                                        <span class="fontsize-16 secondary-font">{{ $userData->phone }}</span>
                                    </div>
                                    <div class="email mb-3">
                                        <span class="fontsize-16 red-font fontweight-9">البريد الإلكتروني :</span>
                                        <span class="fontsize-16 secondary-font">{{ $userData->email }}</span>
                                    </div>

                                </div>
                                <section class="compitition-section py-5">
                                    <div class="container-fluid">
                                        <div class="container">
                                            <div class="row mb-5">
                                                <div class="navigationBar">

                                                    <span class="px-2 fontsize-14"><i class="fas fa-chevron-left"></i></span>
                                                    <span class="secondary-font">  اكمل بياناتك لتتمكن من التسجيل فى المسابقه</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="compitition-form">
                                                        <form class="form-horizontal" enctype="multipart/form-data" action="{{ action('Site\UserController@applayCompetitionsForm') }}" method="post">
                                                            {{ csrf_field() }}




                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">الاسم </label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="name"  value="{{ $userData->name }}">
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">الكود</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="code" disabled  value="{{ count($code) + 1 }}">

                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">الرقم القومى</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="nationalId"  value="{{ $details->nationalId }}" maxlength="14">
                                                                    @if ($errors->has('nationalId'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('nationalId') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">المحافظه</label>
                                                                <div class="col-sm-10">
                                                                    <select name="government" class="form-control form-control-lg" id="exampleFormControlSelect1">
                                                                        <option value="">المحافظه</option>
                                                                        @foreach($mainCity as $mm)
                                                                            <option @if($details->government == $mm->city_ar ) selected @endif
                                                                            value="{{ $mm->city_ar }}"> {{ $mm->city_ar }} </option>
                                                                        @endforeach

                                                                    </select>


                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">المدينه</label>
                                                                <div class="col-sm-10">
                                                                    <select name="city" class="form-control form-control-lg" id="exampleFormControlSelect1">
                                                                        <option value="">المدينه</option>
                                                                        @foreach($city as $cc)
                                                                            <option @if($details->city == $cc->name_ar ) selected @endif value="{{ $cc->name_ar }}"> {{ $cc->name_ar }} </option>
                                                                        @endforeach

                                                                    </select>


                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">المنطقه</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="area"  value="{{ $details->area }}">
                                                                    @if ($errors->has('area'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('area') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">العنوان</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="address"  value="{{ $details->address }}">
                                                                    @if ($errors->has('address'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('address') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">رقم الهاتف (1)</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="phone1"  value="{{ $userData->phone }}">
                                                                    @if ($errors->has('phone1'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('phone1') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">رقم الهاتف(2)</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="phone2"  value="{{ $details->phone2 }}">
                                                                    @if ($errors->has('phone2'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('phone2') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">رقم المنزل</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="phoneHome"  value="{{ $details->phoneHome }}">
                                                                    @if ($errors->has('phoneHome'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('phoneHome') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">الوظيفه</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="job"  value="{{ $details->job }}">
                                                                    @if ($errors->has('job'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('job') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>


                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">النوع</label>
                                                                <div class="col-sm-10">
                                                                    <div class="form-check form-check-inline">
                                                                        <input @if($details->gender == 'ذكر') checked @endif class="form-check-input" type="radio" name="gender" id="" value="ذكر">
                                                                        <label class="form-check-label" for="inlineRadio1">ذكر</label>
                                                                    </div>
                                                                    <div class="form-check form-check-inline">
                                                                        <input @if($details->gender == 'انثى') checked @endif class="form-check-input" type="radio" name="gender" id="" value="انثى">
                                                                        <label class="form-check-label" for="inlineRadio1">انثى</label>
                                                                    </div>

                                                                    @if ($errors->has('gender'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('gender') }}</strong></span>
                                                                    @endif
                                                                </div>

                                                            </div>


                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">تاريخ الميلاد</label>
                                                                <div class="col-sm-10">
                                                                    <input type="date" class="form-control" name="age"  value="{{ $details->age }}">
                                                                    @if ($errors->has('age'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('age') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>


                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">البريد الالكترونى</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="email" readonly value="{{ $userData->email }}">
                                                                    @if ($errors->has('email'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('email') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>


                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">المؤهل الدراسى  </label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" name="education"  value="{{ $details->education }}">
                                                                    @if ($errors->has('education'))
                                                                        <span class="help-block"> <strong style="color: red;">{{ $errors->first('education') }}</strong></span>
                                                                    @endif
                                                                </div>
                                                            </div>


                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">هل تم المشاركه من قبل</label>
                                                                <div class="col-sm-10">
                                                                    <div class="col-sm-10">
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="shareBefore" id="" value="نعم">
                                                                            <label class="form-check-label" for="inlineRadio1">نعم</label>
                                                                        </div>
                                                                        <div class="form-check form-check-inline">
                                                                            <input class="form-check-input" type="radio" name="shareBefore" id="" value="لا">
                                                                            <label class="form-check-label" for="inlineRadio1">لا</label>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="form-group row">
                                                                <label for="staticEmail" class="col-sm-2 col-form-label">الصوره الشخصيه</label>
                                                                <div class="col-sm-10">
                                                                    <input type="file" name="imageName">
                                                                </div>
                                                            </div>





                                                            <div class="text-center mt-4">
                                                                <button type="submit" class="btn readMore-btn">إشتراك</button>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </section>


                            </div>




                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


@endsection