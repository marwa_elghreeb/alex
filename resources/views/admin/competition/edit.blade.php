@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> @lang('admin.Edit item')
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                  action="{{ action('Admin\CompetitionsController@update' , $editData->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">


                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="tabbable-custom ">


                        <ul class="nav nav-tabs ">


                            @foreach($allLang as $data)
                                <li @if($data->symbol == 'ar') class="active" @endif>
                                    <a href="#tab_{{$data->id}}" data-toggle="tab">
                                        <img src="{{ URL ::to ('public/images/'.$data->flag)}}" width="20px;"
                                             height="20px;"/>
                                        {{$data->name}} </a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content">
                            <?php $i =0 ?>
                            @foreach($allLang as $data)
                                <div class="tab-pane @if($data->symbol == 'ar') active @endif" id="tab_{{$data->id}}">
                                    <div class="form-body">
                                        <div class="form-group last">
                                            <label class="control-label col-md-3">  @lang('admin.Competition Details')

                                            </label>
                                            <div class="col-md-6">

                                            </div>
                                        </div>
                                        @if($data->symbol == 'ar')
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">  @lang('admin.Competitions Type')  </label>
                                                <div class="col-md-9">
                                                    <select name="catId" class="form-control ">
                                                        <option value="0" > اختر ... </option>
                                                        @foreach($allType as $type)
                                                            <option value="{{ $type->id }}" @if($editData->catId == $type->id) selected @endif>  {{ $type->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                        @endif
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Name')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="name_{{$data->symbol}}" value="{{ $editData->name }}">

                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Date From')

                                                </label>
                                                <div class="col-md-6">


                                                        <div class="input-group input-medium date date-picker"
                                                             data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                            <input type="text" class="form-control" readonly="" name="dateFrom"
                                                                   value="{{ $editData->dateFrom  }}">
                                                            <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                        </div>



                                                </div>
                                            </div>
                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Date To')

                                                </label>
                                                <div class="col-md-6">

                                                    <div class="input-group input-medium date date-picker"
                                                         data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                                        <input type="text" class="form-control" readonly="" name="dateTo"

                                                               value="{{ $editData->dateTo  }}">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                    </div>




                                                </div>
                                            </div>





                                    </div>

                                </div>
                            @endforeach

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                                        <button type="button" class="btn default"
                                                onclick="window.history.back()">@lang('admin.Cancel')
                                        </button>
                                    </div>
                                </div>
                            </div>
            </form>

        </div>


    </div>


    </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
    </div>


@endsection




