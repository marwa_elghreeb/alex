@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET -->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-pencil"></i>@lang('admin.Edit item')</div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data"
                  action="{{ action('Admin\UserNotesController@update' ,  $userData->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">

                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="form-group">
                        <label class="col-md-3 control-label">@lang('admin.Name') </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="name"
                                   value="{{ $userData->name }}" readonly></div>
                    </div>

                    <hr/>


                    <div class="form-group">
                        <label class="col-md-3 control-label"> اضافه ملاحظات
                        </label>
                        <div class="col-md-9"></div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"> <button type="button" id="addNotes" class="btn blue">
                                @lang('admin.Add new item') </button>
                            <input type="hidden" id="itemCount" value="1">  </label>
                        <div class="col-md-9">

                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th width="30%">التاريخ</th>
                                    <th> الملاحظات</th>
                                    <th></th>
                                </tr>
                                <tbody id="notesContainer">



                                @foreach($userNotes as $note)
                                <tr>
                                    <input type="hidden" name="id[]" value="{{ $note->id }}"/>
                                    <td><div class="input-group input-medium date date-picker"
                                             data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                            <input type="text" class="form-control" readonly="" name="noteDate[]"
                                                   value="{{ $note->noteDate }}">
                                            <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                        </div></td>
                                    <td> <textarea class="form-control" rows="6" name="note[]">
                                            {{ $note->note }}
                                        </textarea></td>
                                    <td>

                                        <a class="btn default removeItemEdit" data-id="{{$note->id}}"
                                           data-url="{{ action('Admin\SettingController@delItem') }}" data-tableName="user_notes">
                                            <i class="fa fa-times"></i> </a>


                                    </td>
                                </tr>
                                    @endforeach

                                <tr>
                                    <td><div class="input-group input-medium date date-picker"
                                             data-date-format="yyyy-mm-dd" data-date-start-date="+0d">
                                            <input type="text" class="form-control" readonly="" name="noteDate[]"
                                                   value="<?php echo date("Y-m-d"); ?>">
                                            <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                        </div></td>
                                    <td> <textarea class="form-control" rows="6" name="note[]"></textarea></td>
                                    <td></td>
                                </tr>



                                </tbody>
                            </table>



                        </div>
                    </div>








                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" class="btn green">@lang('admin.Save') </button>
                            <button type="button" class="btn default"
                                    onclick="window.history.back()">@lang('admin.Cancel')</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->

@endsection




