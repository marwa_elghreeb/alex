@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">  @lang('admin.Show Data')  </span>
                </div>

            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th> @lang('admin.Name')  </th>
                            <th> @lang('admin.Edit') </th>
                            <th> @lang('admin.Delete') </th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allData as $data)
                            @if($data->id == 1)

                                @else
                        <tr class="odd gradeX">
                            <td>
                                #
                            </td>
                            <td> {{ $data->name }} </td>

                            <td>
                                <a href="{{ action('Admin\UserNotesController@show' ,  $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-pencil"></i> </a>

                            </td>


                            <td>

                                <form action="{{ action('Admin\UserNotesController@destroy' , $data->id) }}" method="POST">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btn red"> <i class="fa fa-times"></i></button>
                                </form>

                            </td>

                        </tr>
                        @endif
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>





</div>



@endsection




