@extends('layouts.app')
@section('content')

<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet box  green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>  اضافه عنصر جديد </div>
        <div class="tools"></div>
    </div>
    <div class="portlet-body form">
        <form class="form-horizontal"
              role="form" enctype="multipart/form-data" method="post" action="{{ action('Admin\UserWebController@storeRoot') }}">
            {{ csrf_field() }}
            <div class="form-body">


                <ul>
                    @foreach ($errors->all() as $error)
                         <div class="alert alert-danger"> {{ $error }}</div>
                    @endforeach
                </ul>


                <div class="form-group">
                    <label class="col-md-3 control-label">الرقم التسلسلى </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="serialNo"   value="{{ $serialNo }}"> </div>
                </div>




                <div class="form-group">
                    <label class="col-md-3 control-label">الاسم</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="name" nameAr = "الاسم"  value="{{ old('name') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">الايميل</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}"> </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">رقم الهاتف</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="phone" value="{{ old('phone') }}"> </div>
                </div>



                <div class="form-group ">
                    <label class="control-label col-md-3"> الباسورد</label>
                    <div class="col-md-9">
                        <input type="password" class="form-control" name="password" value="{{ old('password') }}">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">  مجموعات المستخدمين </label>
                    <div class="col-md-9">
                        <select name="groupId" class="form-control select2">

                            @foreach($allGroup as $data)
                                <option value="{{ $data->id }}" > {{ $data->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">امكانيه اضافه مستخدمين</label>
                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" name="addUser" value="1">  اضافه مستخدمين
                                <span></span>
                            </label>

                        </div>
                    </div>
                </div>



            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn green">حفظ </button>
                        <button type="reset" class="btn default" onclick="window.history.back()">الغاء</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->

@endsection




