@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">  عرض البيانات </span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">

                        <div class="btn-group">
                            <a href="{{ action('Admin\CateoryController@create') }}" class="btn sbold green ">
                                <i class="fa fa-plus"></i>  @lang('admin.Add new item')</a>
                        </div>
                    </div>
                    <div class="btn-group">
                        <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-share"></i>
                            <span class="hidden-xs"> الادوات  </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" id="sample_3_tools">
                            <li>
                                <a href="javascript:;" data-action="0" class="tool-action">
                                    <i class="icon-printer"></i> Print</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="1" class="tool-action">
                                    <i class="icon-check"></i> Copy</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="2" class="tool-action">
                                    <i class="icon-doc"></i> PDF</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="3" class="tool-action">
                                    <i class="icon-paper-clip"></i> Excel</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="4" class="tool-action">
                                    <i class="icon-cloud-upload"></i> CSV</a>
                            </li>


                        </ul>
                    </div>
                </div>
            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_3">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th> الاسم  </th>
                            <th> البريد الالكترونى  </th>
                            <th> الهاتف  </th>
                            <th> عدد المستخدمين  </th>
                            <th> العضو الرئيسي  </th>
                            <th> العموله  </th>
                            <th> اضافه اعضاء </th>
                            <th> تعديل </th>
                            <th> تفعيل \ تعطيل </th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allData as $data)
                        <tr class="odd gradeX">
                            <td>
                                #
                            </td>
                            <td> {{ $data->name }}   @if($data->check != 0 )<span style="color: red">  ( شجره)</span> @endif </td>

                            <td>{{ $data->email }}</td>
                            <td>{{ $data->phone }}</td>
                            <td>{{ $data->check }}</td>
                            <td>{{ $data->parentName }}</td>
                            <td>0</td>
                            <td>
                                @if($data->check <  8 )
                                <a href="{{ action('Admin\UserWebController@addUser' ,  $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-users"></i> </a>

                                @else


                                    غير مسموح باضافه اعضاء


                                @endif


                            </td>

                            <td>
                                <a href="{{ action('Admin\UserWebController@edit' ,  $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-pencil"></i> </a>

                            </td>

                            <td>
                                <input type="hidden" id="url" value="{{ action('Admin\UserWebController@adminActive') }}"/>
                                @if($data->isActive == 0)
                                <button type="button" class="btn green-jungle adminActive" data-isActive="1" data-userId="{{ $data->id }}" >قبول </button>
                                <button type="button" class="btn red adminActive" data-isActive="2" data-userId="{{ $data->id }}">رفض </button>
                                    @elseif($data->isActive == 1)
                                    <button type="button" class="btn red adminActive" data-isActive="2" data-userId="{{ $data->id }}" >تعطيل </button>
                                @elseif($data->isActive == 2)
                                    <button type="button" class="btn green-jungle adminActive" data-isActive="1" data-userId="{{ $data->id }}" >تفعيل </button>
                                @endif

                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>





</div>



@endsection




