@extends('layouts.app')
@section('content')

<!-- BEGIN SAMPLE FORM PORTLET -->
<div class="portlet box  green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i> تعديل عنصر</div>
        <div class="tools"></div>
    </div>
    <div class="portlet-body form">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data"
              action="{{ action('Admin\UserWebController@update' ,  $editData->id) }}">
            {{ csrf_field() }}
            {{ method_field('PATCH') }}
            <div class="form-body">

                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger"> {{ $error }}</div>
                    @endforeach
                </ul>


                <div class="form-group">
                    <label class="col-md-3 control-label">الرقم التسلسلى </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="serialNo"   value="{{ $editData->serialNo }}"> </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label"> المستوى </label>
                    <div class="col-md-9">

                        @if($editData->check >= 8)
                            <input type="text" class="form-control"   value="المستوى الاول" readonly>
                            @endif

                        <!--select name="levelNo" id="select2-disabled-inputs-single" class="form-control select2" disabled>
                            <option value="0" @if($editData->levelNo == 0) selected @endif>root</option>
                            <option value="1" @if($editData->levelNo == 1) selected @endif >المستوى الاول</option>
                            <option value="2" @if($editData->levelNo == 2) selected @endif>المستوى الثانى</option>
                            <option value="3" @if($editData->levelNo == 3) selected @endif>المستوى الثالث</option>
                            <option value="4" @if($editData->levelNo == 4) selected @endif>المستوى الرابع</option>
                        </select-->
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label"> مؤسس الشجره </label>
                    <div class="col-md-9">
                        <select name="parentId" class="form-control select2 " >
                            <option value="0" >لا يوجد</option>
                            @foreach($allData as $data)
                                <option value="{{ $data->id }}" @if($data->id == $editData->parentId) selected @endif> {{ $data->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">  المؤسس الرئيسى للشجره </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control"   value="" readonly>
                    </div>
                </div>



                <div class="form-group">
                    <label class="col-md-3 control-label">الاسم</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="name"   value="{{ $editData->name }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">الايميل</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="email" value="{{ $editData->email }}"> </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">رقم الهاتف</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="phone" value="{{ $editData->phone }}"> </div>
                </div>



                <div class="form-group ">
                    <label class="control-label col-md-3"> الباسورد</label>
                    <div class="col-md-9">
                        <input type="password" class="form-control" name="password" value="">

                        <input type="hidden" class="form-control" name="oldPassword" value="{{ $editData->password }}">
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">  مجموعات المستخدمين </label>
                    <div class="col-md-9">
                        <select name="groupId" class="form-control select2">

                            @foreach($allGroup as $data)
                                <option value="{{ $data->id }}" @if($data->id == $editData->groupId) selected @endif> {{ $data->name }}</option>
                            @endforeach

                        </select>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">امكانيه اضافه مستخدمين</label>
                    <div class="col-md-9">
                        <div class="mt-checkbox-inline">
                            <label class="mt-checkbox">
                                <input type="checkbox" name="addUser" value="1" @if($editData->addUser == 1) checked @endif>  اضافه مستخدمين
                                <span></span>
                            </label>

                        </div>
                    </div>
                </div>

                <hr/>
                @if($editData->parentId == 0 || $editData->check > 0)

                    <div class="form-group ">
                        <label class="control-label col-md-3"> الشجره</label>
                        <div class="col-md-9">
                            <h3> {{$editData->name}}</h3>

                            <?php $allParent = \App\UserWeb::where('parentId' , '=' , 0)->get(); ?>
                            @for($i  = 1 ; $i < count($allParent) ; $i++)
                                print_r($i);
                                @for($j=0; $j < $i; $j++)

                                    print_r($j);


                                    @endfor
                                @endfor

                        </div>
                    </div>



                    @endif




            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn green">حفظ </button>
                        <button type="button" class="btn default" onclick="window.history.back()">الغاء</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->

@endsection




