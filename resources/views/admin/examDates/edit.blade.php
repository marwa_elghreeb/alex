@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> @lang('admin.Edit item')
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                  action="{{ action('Admin\ExamesDatesController@update' , $editData->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">


                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="tabbable-custom ">


                        <ul class="nav nav-tabs ">


                            @foreach($allLang as $data)
                                <li @if($data->symbol == 'ar') class="active" @endif>
                                    <a href="#tab_{{$data->id}}" data-toggle="tab">
                                        <img src="{{ URL ::to ('public/images/'.$data->flag)}}" width="20px;"
                                             height="20px;"/>
                                        {{$data->name}} </a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content">


                            @foreach($allLang as $data)


                                <div class="tab-pane @if($data->symbol == 'ar') active @endif" id="tab_{{$data->id}}">

                                    <div class="form-body">


                                        @if($data->symbol == 'ar')


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">  @lang('admin.Competitions Type')  </label>
                                                <div class="col-md-9">
                                                    <select name="competitionsId" class="form-control getLevel select2"
                                                            data-url="{{ action('Admin\CompetitionsController@getLevel') }}">
                                                        <option value="0"> اختر ...</option>
                                                        @foreach($allType as $type)
                                                            <option value="{{ $type->id }}"
                                                            @if($editData->competitionsId  == $type->id) selected @endif>  {{ $type->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">@lang('admin.Competition Levels')</label>
                                                <div class="col-md-9">

                                                    <select name="levelId" id="levelData" class="form-control ">
                                                        @foreach($allLevel as $type)
                                                            <option value="{{ $type->id }}"
                                                                    @if($editData->levelId  == $type->id) selected @endif>
                                                                {{ $type->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>

                                        @endif


                                        <div class="form-group last">
                                            <label class="control-label col-md-3"> اليوم

                                            </label>
                                            <div class="col-md-3">


                                                <div class="input-group input-medium date date-picker"
                                                     data-date-format="yyyy-mm-dd" >
                                                    <input type="text" class="form-control" readonly="" name="dayEx" value="{{ $editData->day }}">
                                                    <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                </div>

                                            <!--input type="text" class="form-control" name="name_{{$data->symbol}}"
                                                       value="{{ old('name_'.$data->symbol) }}"-->

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">من الساعه </label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col col-md-3">
                                                        <div class="input-group">
                                                            <input type="text"
                                                                   class="form-control timepicker timepicker-no-seconds"
                                                                   name="from" value="{{ $editData->from }}">
                                                            <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col col-md-3">
                                                        الى الساعه
                                                    </div>
                                                    <div class="col col-md-3">

                                                        <div class="input-group">
                                                            <input type="text"
                                                                   class="form-control timepicker timepicker-no-seconds"
                                                                   name="to" value="{{ $editData->to }}">
                                                            <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3"> مده الامتحان </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control" name="period" value="{{ $editData->period }}">


                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3"> وقت الاستراحه </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control" name="break" value="{{ $editData->break }}">


                                                </div>
                                            </div>




                                        <div class="form-group">
                                            <label class="control-label col-md-3"> النوع </label>
                                            <div class="col-md-9">
                                                <select class="form-control" name="type">
                                                    <option @if($editData->type  == 'ذكر') selected @endif value="ذكر"> ذكور</option>
                                                    <option @if($editData->type  == 'انثى') selected @endif value="انثى"> اناث</option>
                                                    <option @if($editData->type  == 'مختلط') selected @endif value="مختلط"> مختلطين</option>
                                                </select>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3"> الحد الاقصى للمشاركين </label>
                                            <div class="col-md-9">

                                                <input type="text" class="form-control" name="limit" value="{{ $editData->limit }}">


                                            </div>
                                        </div>


                                    </div>

                                </div>

                            @endforeach


                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                                        <button type="button" class="btn default"
                                                onclick="window.history.back()">@lang('admin.Cancel')
                                        </button>
                                    </div>
                                </div>
                            </div>
            </form>

        </div>


    </div>


    </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
    </div>


@endsection




