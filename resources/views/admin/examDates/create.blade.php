@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> @lang('admin.Add new item')
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                  action="{{ action('Admin\ExamesDatesController@store') }}">
                {{ csrf_field() }}
                <div class="form-body">


                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="tabbable-custom ">


                        <ul class="nav nav-tabs ">


                            @foreach($allLang as $data)
                                <li @if($data->symbol == 'ar') class="active" @endif>
                                    <a href="#tab_{{$data->id}}" data-toggle="tab">
                                        <img src="{{ URL ::to ('public/images/'.$data->flag)}}" width="20px;"
                                             height="20px;"/>
                                        {{$data->name}} </a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content">

                            @foreach($allLang as $data)


                                <div class="tab-pane @if($data->symbol == 'ar') active @endif" id="tab_{{$data->id}}">

                                    <div class="form-body">


                                        @if($data->symbol == 'ar')


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">  @lang('admin.Competitions Type')  </label>
                                                <div class="col-md-9">
                                                    <select name="competitionsId" class="form-control getLevel select2"
                                                            data-url="{{ action('Admin\CompetitionsController@getLevel') }}">
                                                        <option value="0"> اختر ...</option>
                                                        @foreach($allType as $type)
                                                            <option value="{{ $type->id }}">  {{ $type->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">@lang('admin.Competition Levels')</label>
                                                <div class="col-md-9">

                                                    <select name="levelId" id="levelData" class="form-control ">


                                                    </select>
                                                </div>
                                            </div>

                                        @endif


                                        <div class="form-group last">
                                            <label class="control-label col-md-3"> اليوم

                                            </label>
                                            <div class="col-md-3">


                                                <div class="input-group input-medium date date-picker"
                                                     data-date-format="yyyy-mm-dd" >
                                                    <input type="text" class="form-control" readonly="" name="dayEx">
                                                    <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-calendar"></i>
                                                            </button>
                                                        </span>
                                                </div>

                                            <!--input type="text" class="form-control" name="name_{{$data->symbol}}"
                                                       value="{{ old('name_'.$data->symbol) }}"-->

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">من الساعه </label>
                                            <div class="col-md-9">
                                                <div class="row">
                                                    <div class="col col-md-3">
                                                        <div class="input-group">
                                                            <input type="text"
                                                                   class="form-control timepicker timepicker-no-seconds"
                                                                   name="from">
                                                            <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                    <div class="col col-md-3">
                                                        الى الساعه
                                                    </div>
                                                    <div class="col col-md-3">

                                                        <div class="input-group">
                                                            <input type="text"
                                                                   class="form-control timepicker timepicker-no-seconds"
                                                                   name="to">
                                                            <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3"> مده الامتحان (بالساعات)</label>
                                                <div class="col-md-9">

                                                    <input type="number" class="form-control" name="period" value="{{ old('period') }}">


                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3"> وقت الاستراحه (بالدقائق)</label>
                                                <div class="col-md-9">

                                                    <input type="number" class="form-control" name="break" value="{{ old('break') }}">


                                                </div>
                                            </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3"> النوع </label>
                                            <div class="col-md-9">
                                                <select class="form-control" name="type">
                                                   <option value="ذكر"> ذكور</option>
                                                    <option value="انثى"> اناث</option>
                                                    <option value="مختلط"> مختلطين</option>on>
                                                </select>

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3"> الحد الاقصى للمشاركين </label>
                                            <div class="col-md-9">

                                                <input type="text" class="form-control" name="limit" value="{{ old('limit') }}">


                                            </div>
                                        </div>


                                    </div>

                                </div>

                            @endforeach
                        </div>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit" class="btn green">@lang('admin.Save')</button>
                                    <button type="button" class="btn default"
                                            onclick="window.history.back()">@lang('admin.Cancel')
                                    </button>
                                </div>
                            </div>
                        </div>
            </form>

        </div>


    </div>


    </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
    </div>


@endsection




