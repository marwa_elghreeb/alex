@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> @lang('admin.Edit item')
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                  action="{{ action('Admin\CompetitionsConditionController@update' , $catId) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">


                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="tabbable-custom ">


                        <ul class="nav nav-tabs ">


                            @foreach($allLang as $data)
                                <li @if($data->symbol == 'ar') class="active" @endif>
                                    <a href="#tab_{{$data->id}}" data-toggle="tab">
                                        <img src="{{ URL ::to ('public/images/'.$data->flag)}}" width="20px;"
                                             height="20px;"/>
                                        {{$data->name}} </a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content">
                            <?php $i = 0 ?>
                            @foreach($allLang as $data)
                                <div class="tab-pane @if($data->symbol == 'ar') active @endif" id="tab_{{$data->id}}">
                                    <div class="form-body">


                                        @if($data->symbol == 'ar')
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">  @lang('admin.Competitions Type')  </label>
                                                <div class="col-md-9">
                                                    <select name="catId" class="form-control " readonly disabled>
                                                        <option value="0"> اختر ...</option>
                                                        @foreach($allType as $type)
                                                            <option value="{{ $type->id }}"
                                                                    @if($type->id  == $catId) selected @endif>
                                                                {{ $type->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>


                                    @endif






                                    <!--------------شروط المسابقه-------------->
                                        <hr/>
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">
                                                <h3> @lang('admin.Competition Conditions')</h3>
                                                <br/>
                                                <button type="button" id="addSocial"
                                                        class="btn blue"> @lang('admin.Add new item') </button>
                                                <input type="hidden" id="socialCount" value="1">

                                            </label>
                                            <div class="col-md-9">
                                                <table class="table table-bordered table-hover">

                                                    <th>@lang('admin.OrderNo')</th>
                                                    <th>@lang('admin.Name') </th>
                                                    <th>@lang('admin.Descrption')</th>
                                                    <th></th>

                                                    <tbody id="productContiner">
                                                    @foreach($editData as $data)
                                                        <tr>
                                                            <input type="hidden" name="id[]" value="{{ $data->id }}">
                                                            <td><input type="text" class="form-control"
                                                                       name="name[]" value="{{ $data->orderNo }}">
                                                            </td>
                                                            <td><input type="text" class="form-control " name="icon[]"
                                                                       value="{{ $data->name }}">
                                                            </td>
                                                            <td><input type="text" class="form-control" name="link[]"
                                                                       value="{{ $data->title }}">
                                                            </td>
                                                            <td><a class="btn default removeItemEdit" data-id="{{$data->id}}"
                                                                   data-url="{{ action('Admin\SettingController@delItem') }}"
                                                                   data-tableName="competitions_conditions"><i
                                                                            class="fa fa-times"></i> </a></td>
                                                        </tr>
                                                    @endforeach


                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>


                                    </div>
                                    @endforeach

                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-4 col-md-8">
                                                <button type="submit" class="btn green">@lang('admin.Save') </button>
                                                <button type="button" class="btn default"
                                                        onclick="window.history.back()">@lang('admin.Cancel')
                                                </button>
                                            </div>
                                        </div>
                                    </div>
            </form>

        </div>


    </div>


    </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
    </div>


@endsection




