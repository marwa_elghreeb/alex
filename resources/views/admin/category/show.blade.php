@extends('layouts.app')
@section('content')


    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-social-dribbble font-blue-sharp"></i>
                        <span class="caption-subject font-blue-sharp bold uppercase">اسئله المسابقه</span>
                    </div>
                    <div class="actions">


                    </div>
                </div>
                <div class="portlet-body">
                    <div id="tree_1" class="tree-demo">
                        <ul>



                            @foreach($compLevel as $data)
                            <li> <a href="{{ action('Admin\CompetitionsLevelController@edit' , $data->id) }}"> {{ $data->name }} </a>


                                <ul>
                                    @foreach($data->exams as $sub)
                                        <?php  $nameArr = json_decode($sub->name, true);?>
                                    <li ><a href="{{ action('Admin\ExamsController@edit' , $sub->id) }}">{{ $nameArr[Lang::getLocale()] }} </a></li>
                                        @endforeach

                                </ul>

                            </li>
                                @endforeach



                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection




