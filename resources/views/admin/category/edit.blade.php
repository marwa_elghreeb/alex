@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> @lang('admin.Edit item')
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                  action="{{ action('Admin\CateoryController@update' , $main->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">


                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="tabbable-custom ">


                        <ul class="nav nav-tabs ">


                            @foreach($allLang as $data)
                                <li @if($data->symbol == 'ar') class="active" @endif>
                                    <a href="#tab_{{$data->id}}" data-toggle="tab">
                                        <img src="{{ URL ::to ('public/images/'.$data->flag)}}" width="20px;"
                                             height="20px;"/>
                                        {{$data->name}} </a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content">
                            <?php $i =0 ?>
                            @foreach($allLang as $data)
                                <div class="tab-pane @if($data->symbol == 'ar') active @endif" id="tab_{{$data->id}}">
                                    <div class="form-body">






                                        <div class="form-group last">
                                            <label class="control-label col-md-3">  @lang('admin.Name')

                                            </label>
                                            <div class="col-md-9">

                                                <input type="text" class="form-control"  name="name_{{$data->symbol}}"

                                                value="{{ $nameArr['ar'] }}"  >

                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-3 control-label"> اسم المحافظه </label>
                                            <div class="col-md-9">
                                                <select name="city[]" id="multiple" class="form-control select2-multiple" multiple>
                                                    <option>@lang('admin.Choose') </option>
                                                    @foreach($allCity as $city)
                                                        <option value="{{ $city->city_ar }}"
                                                                @if(in_array($city->city_ar, $catCity)) selected @endif> {{ $city->city_ar }}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                        </div>


                                        <div class="form-group last">
                                            <label class="control-label col-md-3"> ادنى سن

                                            </label>
                                            <div class="col-md-9">

                                                <input type="text" class="form-control"  name="minAge"

                                                       value="{{ $main->minAge }}"  >

                                            </div>
                                        </div>



                                        <div class="form-group last">
                                            <label class="control-label col-md-3">  اقصى سن

                                            </label>
                                            <div class="col-md-9">

                                                <input type="text" class="form-control"  name="maxAge"

                                                       value="{{ $main->maxAge }}"  >

                                            </div>
                                        </div>




                                    <!--div class="form-group last">
                                            <label class="control-label col-md-3">  @lang('admin.Tittel')

                                            </label>
                                            <div class="col-md-9">
                                                <input type="text" class="form-control"  name="tittle_{{$data->symbol}}" value="{{ $tittleArr['ar'] }}"  >



                                            </div>
                                        </div-->



                                        <div class="form-group ">
                                            <label class="control-label col-md-3"> @lang('admin.Image')</label>
                                            <div class="col-md-9">

                                                @foreach($imgArr as $img)
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-preview thumbnail"
                                                         data-trigger="fileinput"
                                                         style="width: 200px; height: 150px;">

                                                        <img src="{{ URL ::to ('public/images/'.$img)}}"/>
                                                    </div>
                                                    <div>
                               <span class="btn red btn-outline btn-file">
                                   <span class="fileinput-new"> Select image </span>
                                   <span class="fileinput-exists"> Change </span>
                                   <input type="file" name="imageName[]"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists"
                                                           data-dismiss="fileinput"> Remove </a>
                                                    </div>
                                                </div>
                                                    @endforeach



                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <label class="control-label col-md-3">الفيديوهات</label>
                                            <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                        <span class="btn green btn-file">
                                                            <span class="fileinput-new"> Select file </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="video[]" multiple> </span>
                                                    <span class="fileinput-filename"> </span> &nbsp;
                                                    <a href="javascript:;" class="close fileinput-exists" data-dismiss="fileinput"> </a>
                                                </div>
                                            </div>
                                        </div>





                                    </div>

                                </div>
                                    <?php  $i++ ?>
                            @endforeach

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                                        <button type="button" class="btn default"
                                                onclick="window.history.back()">@lang('admin.Cancel')
                                        </button>
                                    </div>
                                </div>
                            </div>
            </form>

        </div>


    </div>


    </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
    </div>


@endsection




