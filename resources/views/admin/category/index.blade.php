@extends('layouts.app')
@section('content')


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase"> @lang('admin.Show Data') </span>
                </div>

                <div class="actions">
                    <div class="btn-group">
                        <a href="{{ action('Admin\CateoryController@create') }}" class="btn sbold green ">
                            <i class="fa fa-plus"></i>  @lang('admin.Add new item')</a>
                    </div>

                    <div class="btn-group btn-group-devided" data-toggle="buttons">


                    </div>
                    <div class="btn-group">
                        <a class="btn red btn-outline btn-circle" href="javascript:;" data-toggle="dropdown">
                            <i class="fa fa-share"></i>
                            <span class="hidden-xs"> الادوات  </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu pull-right" id="sample_3_tools">
                            <li>
                                <a href="javascript:;" data-action="0" class="tool-action">
                                    <i class="icon-printer"></i> Print</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="1" class="tool-action">
                                    <i class="icon-check"></i> Copy</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="2" class="tool-action">
                                    <i class="icon-doc"></i> PDF</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="3" class="tool-action">
                                    <i class="icon-paper-clip"></i> Excel</a>
                            </li>
                            <li>
                                <a href="javascript:;" data-action="4" class="tool-action">
                                    <i class="icon-cloud-upload"></i> CSV</a>
                            </li>


                        </ul>
                    </div>
                </div>

            </div>
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover" id="sample_3">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th> @lang('admin.Name')  </th>
                            <th> الشروط </th>
                            <th> المستويات والجوائز </th>
                            <th> الممتحنين </th>
                            <th> البنود </th>
                            <th> المتقدمين للمسابقه </th>
                            <th>  اسئله المسابقه </th>

                            <th> @lang('admin.Edit') </th>
                            <!--th> @lang('admin.Delete') </th-->
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i =1 ; ?>
                        @foreach($category as $data)
                        <tr class="odd gradeX">
                            <td>
                               {{ $i }}
                            </td>
                            <td> {{ $data->name }} </td>
                            <td>

                                @if($data->id == 3)
                                    @else
                                <a href="{{ action('Admin\CompetitionsConditionController@edit' ,  $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-list"></i> </a>
                                    @endif

                            </td>

                            <td>
                                <a href="{{ action('Admin\CompetitionsLevelController@index') }}" class="btn sbold blue ">
                                    <i class="fa fa-list"></i> </a>

                            </td>

                            <td>
                                @if($data->id == 3)
                                @else
                                <a href="{{ action('Admin\ExaminerController@show' , $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-users"></i> </a>
                                    @endif

                            </td>


                            <td>
                                @if($data->id == 3)
                                @else
                                <a href="{{ action('Admin\CompetitionsBandController@edit' ,  $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-list"></i> </a>
                                    @endif

                            </td>


                            <td> {{ $data->countUser }}

                                <a href="{{ action('Admin\UserCompetitionsControllers@show' , $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-user"></i> </a>
                            </td>


                            <td>
                                @if($data->id == 3)
                                @else
                                    <a href="{{ action('Admin\CateoryController@show' ,  $data->id) }}" class="btn sbold blue ">
                                        <i class="fa fa-question "></i> </a>
                                @endif

                            </td>


                            <td>
                                <a href="{{ action('Admin\CateoryController@edit' ,  $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-pencil"></i> </a>

                            </td>

                            <!--td>
                                



                                          <form action="{{ action('Admin\CateoryController@destroy' , $data->id) }}" method="POST">
                                        @method('DELETE')
                        @csrf
                                <button type="submit" class="btn red"> <i class="fa fa-times"></i></button>
                            </form>



                    </td-->
                        </tr>
                            <?php $i ++; ?>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>



@endsection




