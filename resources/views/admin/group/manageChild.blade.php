<ul class="sub-menu">
    @foreach($childs as $child)
        <li class="nav-item ">
            <input type="checkbox" name="menu[]" value="{{  $child->id }}"
                   @if($groupMenu->contains('menuId', $child->id )) checked="checked" @endif
            >{{ $child->name_ar }}
            @if(count($child->childs))
                @include('admin.menu.manageChild',['childs' => $child->childs])
            @endif
        </li>
    @endforeach
</ul>