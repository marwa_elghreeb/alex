@extends('layouts.app')
@section('content')

<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet box  green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i> @lang('admin.Add new item')  </div>
        <div class="tools"></div>
    </div>
    <div class="portlet-body form">
        <form class="form-horizontal"
              role="form" enctype="multipart/form-data" method="post" action="{{ action('Admin\UsergroupsController@store') }}">
            {{ csrf_field() }}
            <div class="form-body">


                <ul>
                    @foreach ($errors->all() as $error)
                         <div class="alert alert-danger"> {{ $error }}</div>
                    @endforeach
                </ul>



                <div class="form-group">
                    <label class="col-md-3 control-label">@lang('admin.Name') </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="name" nameAr = "الاسم"  value="{{ old('name') }}"> </div>
                </div>

                <hr/>

                 <div class="form-group">
                    <label class="col-md-3 control-label">@lang('admin.Permission') </label>
                    <div class="col-md-9">


                        <div class="mt-checkbox-list">
                            <!--ul class="nested" >
                            @foreach($allMenu as $menu)
                                <li> <input type="checkbox" name="menu[]" value="{{  $menu->id }}" > {{ $menu->name_ar  }}   </li>
                                @endforeach
                                </ul-->



                                <ul class="nested">
                                    @foreach($allMenu as $category)
                                        <li>
                                            <input type="hidden" name="menu[]" value="{{ $menu->id }}" >

                                            <input type="checkbox" name="" value="1" >

                                            {{ $category->name_ar }}
                                                @if(count($category->childs))
                                                    @include('admin.group.manageChild',['childs' => $category->childs])
                                                @endif
                                        </li>
                                    @endforeach
                                </ul>

                        </div>


                    </div>
                </div>


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                        <button type="reset" class="btn default" onclick="window.history.back()">@lang('admin.Cancel')</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->

@endsection




