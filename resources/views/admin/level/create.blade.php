@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> @lang('admin.Add new item')
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                  action="{{ action('Admin\CompetitionsLevelController@store') }}">
                {{ csrf_field() }}
                <div class="form-body">


                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="tabbable-custom ">


                        <ul class="nav nav-tabs ">


                            @foreach($allLang as $data)
                                <li @if($data->symbol == 'ar') class="active" @endif>
                                    <a href="#tab_{{$data->id}}" data-toggle="tab">
                                        <img src="{{ URL ::to ('public/images/'.$data->flag)}}" width="20px;"
                                             height="20px;"/>
                                        {{$data->name}} </a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content">

                            @foreach($allLang as $data)


                                <div class="tab-pane @if($data->symbol == 'ar') active @endif" id="tab_{{$data->id}}">

                                    <div class="form-body">


                                        @if($data->symbol == 'ar')


                                            <div class="form-group">
                                                <label class="col-md-3 control-label">  @lang('admin.Competitions Type')  </label>
                                                <div class="col-md-9">
                                                    <select name="typeId" class="form-control ">
                                                        <option value="" > اختر ... </option>
                                                        @foreach($allType as $type)
                                                            <option value="{{ $type->id }}">  {{ $type->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.OrderNo')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="orderNo" value="{{ old('orderNo') }}">

                                                </div>
                                            </div>




                                        @endif


                                        <div class="form-group last">
                                            <label class="control-label col-md-3">  @lang('admin.Name')

                                            </label>
                                            <div class="col-md-9">

                                                <input type="text" class="form-control"  name="name_{{$data->symbol}}" value="{{ old('name_'.$data->symbol) }}">

                                            </div>
                                        </div>


                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Parts')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="parts" value="{{ old('parts') }}">





                                                </div>
                                            </div>
                                        <!--=========================-->
                                            <hr/>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"> <h3> @lang('admin.Awards')</h3>
                                                    <br/>
                                                    <button type="button" id="addSocial" class="btn blue"> @lang('admin.Add new item') </button>
                                                    <input type="hidden" id="socialCount" value="1">

                                                </label>
                                                <div class="col-md-9">
                                                    <table class="table table-bordered table-hover">

                                                        <th>@lang('admin.Center')</th>
                                                        <th>@lang('admin.The value') </th>
                                                        <th>@lang('admin.The value of the prize')</th>
                                                        <th></th>

                                                        <tbody id="productContiner">
                                                            <tr>
                                                                <td><input type="text" class="form-control" name="name[]"></td>
                                                                <td><input type="text" class="form-control " name="icon[]" ></td>
                                                                <td><input type="text" class="form-control" name="link[]"></td>
                                                                <td><a class="btn default removeItem" ><i class="fa fa-times"></i> </a></td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>



                                            <!--=========================-->
                                            <hr/>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"> <h3> القراءات</h3>
                                                    <br/>
                                                    <button type="button" id="addItem" class="btn blue">
                                                        @lang('admin.Add new item') </button>
                                                    <input type="hidden" id="itemCount" value="1">

                                                </label>
                                                <div class="col-md-9">
                                                    <table class="table table-bordered table-hover">

                                                        <th>الاسم</th>

                                                        <th></th>

                                                        <tbody id="itemContiner">
                                                        <tr>
                                                            <td><input type="text" class="form-control" name="itemName[]"></td>

                                                            <td><a class="btn default removeItem" ><i class="fa fa-times"></i> </a></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>







                                    </div>

                                </div>

                            @endforeach

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" class="btn green">@lang('admin.Save')</button>
                                        <button type="button" class="btn default"
                                                onclick="window.history.back()">@lang('admin.Cancel')
                                        </button>
                                    </div>
                                </div>
                            </div>
            </form>

        </div>


    </div>


    </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
    </div>


@endsection




