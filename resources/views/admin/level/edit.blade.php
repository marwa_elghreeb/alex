@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> @lang('admin.Edit item')
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                  action="{{ action('Admin\CompetitionsLevelController@update' , $editData->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">


                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="tabbable-custom ">


                        <ul class="nav nav-tabs ">


                            @foreach($allLang as $data)
                                <li @if($data->symbol == 'ar') class="active" @endif>
                                    <a href="#tab_{{$data->id}}" data-toggle="tab">
                                        <img src="{{ URL ::to ('public/images/'.$data->flag)}}" width="20px;"
                                             height="20px;"/>
                                        {{$data->name}} </a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content">
                            <?php $i =0 ?>
                            @foreach($allLang as $data)
                                <div class="tab-pane @if($data->symbol == 'ar') active @endif" id="tab_{{$data->id}}">
                                    <div class="form-body">
                                        @if($data->symbol == 'ar')
                                            <div class="form-group">
                                                <label class="col-md-3 control-label">  @lang('admin.Competitions Type')  </label>
                                                <div class="col-md-9">
                                                    <select name="typeId" class="form-control ">
                                                        <option value="0" > اختر ... </option>
                                                        @foreach($allType as $type)
                                                            <option value="{{ $type->id }}" @if($editData->typeId == $type->id) selected @endif>  {{ $type->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.OrderNo')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="orderNo" value="{{ $editData->orderNo  }}">

                                                </div>
                                            </div>


                                        @endif



                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Name')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="name_{{$data->symbol}}" value="{{ $editData->name }}">

                                                </div>
                                            </div>


                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Parts')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="parts" value="{{ $editData->parts }}">
                                                </div>
                                            </div>


                                            <!--=========================-->
                                            <hr/>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"> <h3> @lang('admin.Awards')</h3>
                                                    <br/>
                                                    <button type="button" id="addSocial" class="btn blue"> @lang('admin.Add new item') </button>
                                                    <input type="hidden" id="socialCount" value="1">

                                                </label>
                                                <div class="col-md-9">
                                                    <table class="table table-bordered table-hover">

                                                        <th>@lang('admin.Center')</th>
                                                        <th>@lang('admin.The value') </th>
                                                        <th>@lang('admin.The value of the prize')</th>
                                                        <th></th>

                                                        <tbody id="productContiner">
                                                        @foreach($subItem as $sub)
                                                            <tr>
                                                                <input type="hidden"  name="id[]" value="{{$sub->id}}">
                                                                <td><input type="text" class="form-control" name="name[]" value="{{ $sub->name }}"></td>
                                                                <td><input type="text" class="form-control " name="icon[]" value="{{ $sub->title }}"></td>
                                                                <td><input type="text" class="form-control" name="link[]" value="{{ $sub->content }}"></td>
                                                                <td><a class="btn default removeItemEdit" data-id="{{$sub->id}}"
                                                                       data-url="{{ action('Admin\SettingController@delItem') }}"
                                                                       data-tableName="awards"><i class="fa fa-times"></i> </a></td>
                                                            </tr>
                                                        @endforeach


                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>

                                            <!--=========================-->
                                            <hr/>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"> <h3> القراءات</h3>
                                                    <br/>
                                                    <button type="button" id="addItem" class="btn blue">
                                                        @lang('admin.Add new item') </button>
                                                    <input type="hidden" id="itemCount" value="1">

                                                </label>
                                                <div class="col-md-9">
                                                    <table class="table table-bordered table-hover">

                                                        <th>الاسم</th>

                                                        <th></th>

                                                        <tbody id="itemContiner">

                                                        @foreach($reades as $read)
                                                            <tr>
                                                                <input type="hidden"  name="itemId[]" value="{{$read->id}}">
                                                                <td><input type="text" class="form-control" name="itemName[]" value="{{ $read->name }}"></td>

                                                                <td><a class="btn default removeItemEdit" data-id="{{$read->id}}"
                                                                       data-url="{{ action('Admin\SettingController@delItem') }}"
                                                                       data-tableName="competitions_readers"><i class="fa fa-times"></i> </a></td>
                                                            </tr>
                                                        @endforeach

                                                        </tbody>
                                                    </table>

                                                </div>
                                            </div>



                                    </div>

                                </div>
                            @endforeach

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                                        <button type="button" class="btn default"
                                                onclick="window.history.back()">@lang('admin.Cancel')
                                        </button>
                                    </div>
                                </div>
                            </div>
            </form>

        </div>


    </div>


    </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
    </div>


@endsection




