@extends('layouts.app')
@section('content')

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-dark">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject bold uppercase">  عرض البيانات</span>
                </div>

            </div>
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-8"> </div>
                        <div class="col-md-2">

                            <div class="btn-group">
                                <a href="{{ action('Admin\MessageUsersController@edit' , 1) }}" class="btn sbold green ">
                                    <i class="fa fa-plus"></i>  ارسال رساله للمشرفين</a>
                            </div>

                        </div>

                        <div class="col-md-2">

                            <div class="btn-group">
                                <a href="{{ action('Admin\MessageUsersController@edit' , 2) }}" class="btn sbold green ">
                                    <i class="fa fa-plus"></i>  ارسال رساله للمستخدمين</a>
                            </div>

                        </div>
                    </div>
                </div>
                <table class="table table-striped table-bordered table-hover" id="sample_3">
                    <thead>
                        <tr>
                            <th>
                                #
                            </th>
                            <th>  اسم الراسل   </th>
                            <th> اسم المرسل </th>
                            <th> التاريخ </th>
                            <th> التفاصيل </th>

                        </tr>
                    </thead>
                    <tbody>
                        @foreach($allData as $data)
                        <tr class="odd gradeX">
                            <td>
                                #
                            </td>
                            <td>
                                @if($data->senderType == 'admin')
                                    Admin
                                    @else
                                {{ $data->SenderData->name }}
                                    @endif


                            </td>
                            <td>
                                @if($data->recipientType == 'admin')
                                    Admin
                                @else
                                    {{ $data->RecipientData->name }}
                                @endif


                                </td>
                            <td> <?php echo date("Y-m-d" , strtotime ($data->created_at)); ?> </td>
                            <td>
                                <a href="{{ action('Admin\MessageUsersController@show' ,  $data->id) }}" class="btn sbold blue ">
                                    <i class="fa fa-pencil"></i> </a>

                            </td>

                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>



@endsection




