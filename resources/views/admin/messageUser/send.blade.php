@extends('layouts.app')
@section('content')

<!-- BEGIN SAMPLE FORM PORTLET -->
<div class="portlet box  green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-pencil"></i>ارسال رساله</div>
        <div class="tools"></div>
    </div>
    <div class="portlet-body form">
        <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data"
              action="{{ action('Admin\MessageUsersController@store'   ) }}">
            {{ csrf_field() }}

            <div class="form-body">

                <ul>
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger"> {{ $error }}</div>
                    @endforeach
                </ul>


                <div class="form-group last">
                    <label class="control-label col-md-3">  @lang('admin.Name')

                    </label>
                    <div class="col-md-9">

                        <select  class="form-control select2" name="recipientId" >
                           <option value=""> اختر </option>
                            @foreach($allUser as $data)
                                <option value="{{ $data->id }}"  > {{ $data->name }}</option>
                            @endforeach

                        </select>



                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">الرساله </label>
                    <div class="col-md-9">

                        <textarea class="form-control" name="message" > </textarea>



                    </div>
                </div>








            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                        <button type="button" class="btn default" onclick="window.history.back()">@lang('admin.Cancel')</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->

@endsection




