@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-12 ">
            <!-- BEGIN PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-bubble font-hide hide"></i>
                        <span class="caption-subject font-hide bold uppercase">تفاصيل المحادثه</span>
                    </div>
                    <div class="actions">
                        <div class="portlet-input input-inline">
                            <div class="input-icon right">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="portlet-body" id="chats">
                    <div class="slimScrollDiv"
                         style="position: relative; overflow: hidden; width: auto; height: 525px;">
                        <div class="scroller" style="height: 525px; overflow: hidden; width: auto;"
                             data-always-visible="1" data-rail-visible1="1" data-initialized="1">
                            <ul class="chats">


                                @foreach($details as $del)

                                    @if($del->userId == Auth::user()->id)

                                        <li class="in">  <!-- mine -->
                                            <img class="avatar" alt=""
                                                 src="{{ URL ::to ('assets/admin/ar/assets/layouts/layout/img/avatar.png')}}">
                                            <div class="message">

                                                <a href="javascript:;" class="name"> admin  </a>
                                                <span class="datetime"> <?php echo date("h:i A " , strtotime ($del->created_at)); ?> </span>
                                                <span class="body"> {{ $del->message }}
                                    </span>
                                            </div>
                                        </li>
                                    @else

                                        <li class="out">
                                            <img class="avatar" alt=""
                                                 src="{{ URL ::to ('assets/admin/ar/assets/layouts/layout/img/avatar.png')}}">
                                            <div class="message">
                                                <a href="javascript:;" class="name"> Bob Nilson </a>
                                                <span class="datetime"> <?php echo date("h:i A " , strtotime ($del->created_at)); ?> </span>
                                                <span class="body"> {{ $del->message }} </span>
                                            </div>
                                        </li>
                                    @endif

                                @endforeach


                            </ul>
                        </div>
                        <div class="slimScrollBar"
                             style="background: rgb(187, 187, 187); width: 7px; position: absolute;
                               top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99;
                               left: 1px; height: 311.089px;"></div>
                        <div class="slimScrollRail" style="width:
                               7px; height: 100%; position: absolute; top: 0px; display: none; border-radius:
                                7px; background: rgb(234, 234, 234); opacity: 0.2; z-index: 90; left: 1px;"></div>
                    </div>
                    <div class="chat-form">
                        <div class="input-cont">
                            <input class="form-control" type="text" placeholder="اكتب رسالتك هنا..."></div>
                        <div class="btn-cont">
                            <span class="arrow"> </span>
                            <a href="" class="btn blue icn-only" id="sendMessage">

                                <i class="fa fa-check icon-white"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END PORTLET-->
        </div>

    </div>

@endsection




