@extends('layouts.app')
@section('content')

<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet box  green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>   @lang('admin.Add new item') </div>
        <div class="tools"></div>
    </div>
    <div class="portlet-body form">
        <form class="form-horizontal"
              role="form" enctype="multipart/form-data" method="post" action="{{ action('Admin\CompetitorsController@store') }}">
            {{ csrf_field() }}
            <div class="form-body">


                <ul>
                    @foreach ($errors->all() as $error)
                         <div class="alert alert-danger"> {{ $error }}</div>
                    @endforeach
                </ul>



                <div class="form-group">
                    <label class="col-md-3 control-label">@lang('admin.Name')</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="userName"   value="{{ old('userName') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">الرقم القومى</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" maxlength="14" name="nationalId" value="{{ old('nationalId') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label"> المدينه</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="city" value="{{ old('city') }}"> </div>
                </div>



                <div class="form-group">
                    <label class="col-md-3 control-label"> المنطقه</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="area" value="{{ old('area') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label"> العنوان</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="address" value="{{ old('address') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label"> رقم الهاتف (1)</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" maxlength="11"  name="phone1" value="{{ old('phone1') }}"> </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label"> رقم الهاتف (2)</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" maxlength="11" name="phone2" value="{{ old('phone2') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">  رقم المنزل</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control"  name="phoneHome" value="{{ old('phoneHome') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">  البريد الالكترونى </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="email" value="{{ old('email') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">   المؤهل الدراسى </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="education" value="{{ old('education') }}"> </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">    المهنه </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="job" value="{{ old('job') }}"> </div>
                </div>



                <div class="form-group">
                    <label class="col-md-3 control-label">    الجنس </label>
                    <div class="col-md-9">
                        <select class="form-control select2" name="gender">
                            <option value="ذكر">ذكر</option>
                            <option value="انثى">انثى</option>
                        </select>

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">    العمر </label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" maxlength="2" name="age" value="{{ old('age') }}"> </div>
                </div>



                <div class="form-group">
                    <label class="col-md-3 control-label">    هل تم المشاركه من قبل  </label>
                    <div class="col-md-9">

                        <select class="form-control select2" name="shareBefore">
                            <option value="نعم">نعم</option>
                            <option value="لا">لا</option>
                        </select>


                    </div>
                </div>


                <div class="form-group ">
                    <label class="control-label col-md-3"> @lang('admin.Image')</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-preview thumbnail"
                                 data-trigger="fileinput"
                                 style="width: 200px; height: 150px;"></div>
                            <div>
                               <span class="btn red btn-outline btn-file">
                                   <span class="fileinput-new"> Select image </span>
                                   <span class="fileinput-exists"> Change </span>
                                   <input type="file" name="imageName"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists"
                                   data-dismiss="fileinput"> Remove </a>
                            </div>
                        </div>

                    </div>
                </div>



            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                        <button type="reset" class="btn default" onclick="window.history.back()">@lang('admin.Cancel')</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->

@endsection




