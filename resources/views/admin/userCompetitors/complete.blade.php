@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET -->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-pencil"></i>@lang('admin.Edit item')</div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data"
                  action="{{ action('Admin\UserCompetitionsControllers@update' ,  $editData->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">

                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>

                    <div class="form-group">
                        <label class="col-md-3 control-label">كود المستخدم</label>
                        <div class="col-md-9">
                            <input type="text" name="code" class="form-control" readonly   value="{{ $editData->id  }}"> </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">@lang('admin.Name')</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="userName"   value="{{ $editData->userName  }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">الرقم القومى</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" maxlength="14" name="nationalId" value="{{  $editData->nationalId   }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"> المدينه</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="city" value="{{ $editData->city  }}"> </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-3 control-label"> المنطقه</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="area" value="{{ $editData->area  }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"> العنوان</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="address" value="{{ $editData->address  }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label"> رقم الهاتف (1)</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" maxlength="11" name="phone1" value="{{ $editData->phone1  }}"> </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label"> رقم الهاتف (2)</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" maxlength="11" name="phone2" value="{{ $editData->phone2  }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">  رقم المنزل</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="phoneHome" value="{{ $editData->phoneHome  }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">  البريد الالكترونى </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="email" value="{{ $editData->email  }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">   المؤهل الدراسى </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="education" value="{{ $editData->education }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">    المهنه </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="job" value="{{ $editData->job }}"> </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-3 control-label">    الجنس </label>
                        <div class="col-md-9">
                            <select class="form-control select2" name="gender">
                                <option value="ذكر" @if($editData->gender == 'ذكر') @endif>ذكر</option>
                                <option value="انثى"  @if($editData->gender == 'انثى') @endif>انثى</option>
                            </select>

                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">    العمر </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" maxlength="2" name="age" value="{{ $editData->age  }}"> </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-3 control-label">    هل تم المشاركه من قبل  </label>
                        <div class="col-md-9">

                            <select class="form-control select2" name="shareBefore">
                                <option value="نعم" @if($editData->shareBefore == 'نعم') @endif>نعم</option>
                                <option value="لا" @if($editData->shareBefore == 'لا') @endif>لا</option>
                            </select>


                        </div>
                    </div>

                    <input type="hidden" name="oldImage" value="{{ $editData->imageName }}">
                    <div class="form-group ">
                        <label class="control-label col-md-3"> @lang('admin.Image')</label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail"
                                     data-trigger="fileinput"
                                     style="width: 200px; height: 150px;">

                                    <img src="{{ URL ::to ('public/images/'.$editData->imageName)}}"/>
                                </div>
                                <div>
                               <span class="btn red btn-outline btn-file">
                                   <span class="fileinput-new"> Select image </span>
                                   <span class="fileinput-exists"> Change </span>
                                   <input type="file" name="imageName"> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists"
                                       data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>

                        </div>
                    </div>
                    <hr/>
                    <div class="form-group">
                        <label class="col-md-3 control-label">    بيانات التحفيظ </label>
                        <div class="col-md-9"> </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">     اسم المحفظ </label>
                        <div class="col-md-9">
                            <select name="teatcherId" class="form-control select2">

                                <option> اختر ... </option>
                                @foreach($all as $data)
                                    <option value="{{ $data->id }}" @if($details->teatcherId  == $data->id) selected @endif> {{ $data->name }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">    الرقم الوظيفى </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control"  name="jobCode" value="{{ $details->jobCode  }}"

                                   onkeypress="return isNumberKey(event)"
                            > </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">     القطاع والاداره </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control"  name="job" value="{{ $details->job  }}"> </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label">      الصله بالموظف </label>
                        <div class="col-md-9">
                            <select name="relatedTo" class="form-control select2">
                            <option> اختر ... </option>
                            <option value="نفسه">  نفسه </option>
                            <option value="ابن الموظف ">  ابن الموظف </option>
                            <option value="بنت الموظف">  بنت الموظف </option>
                            <option value="زوجه الموظف">  زوجه الموظف </option>
                            <option value="والده الموظف">  والده الموظف </option>
                            <option value="والد الموظف">  والد الموظف </option>
                            </select>
                            </div>
                    </div>



                    <div class="form-group">
                        <label class="col-md-3 control-label">    مستوى المسابقه </label>
                        <div class="col-md-9">
                            <select name="level" class="form-control select2">

                                <option> اختر ... </option>
                                @foreach($level as $data)
                                    <option value="{{ $data->id }}"
                                            @if($details->level  == $data->id) selected @endif> {{ $data->name }}</option>
                                @endforeach

                            </select>

                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">     القراءات </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control"  name="readType" value="{{ $details->readType  }}"> </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">     الصور </label>
                        <div class="col-md-9">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-preview thumbnail"
                                     data-trigger="fileinput"
                                     style="width: 200px; height: 150px;">

                                </div>
                                <div>
                               <span class="btn red btn-outline btn-file">
                                   <span class="fileinput-new"> Select image </span>
                                   <span class="fileinput-exists"> Change </span>
                                   <input type="file" name="images[]" multiple> </span>
                                    <a href="javascript:;" class="btn red fileinput-exists"
                                       data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>


                        </div>
                    </div>





                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" class="btn green">@lang('admin.Save') </button>
                            <button type="button" class="btn default" onclick="window.history.back()">@lang('admin.Cancel')</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->

@endsection




