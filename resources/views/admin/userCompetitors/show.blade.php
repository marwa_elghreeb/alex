@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-md-6">
            <span class="caption-subject bold uppercase">  المسابقه الحاليه ({{$subName}})  </span>
        </div>
        <div class="col-md-6">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">  عرض بيانات المتسابقين فى ({{$catName}})  </span>
                    </div>

                </div>
                <div class="portlet-body">


                    <table class="table table-striped table-bordered table-hover table-checkable order-column"
                           id="sample_1">
                        <thead>
                        <tr>
                            <th>
                                #
                            </th>



                            <th>  المستوى </th>
                            <th> @lang('admin.Name')  </th>
                            <th> رقم الهاتف</th>
                            @if($id == 3)
                                <th>  استكمال بيانات المتسابق</th>
                                @endif

                            <th>  عرض بيانات المتسابق </th>

                            <th>الحاله </th>

                        </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($allData as $data)
                            <tr class="odd gradeX">
                                <td>
                                    {{ $i }}
                                    <!--input class="addComp" type="checkbox"  @if($data->flag == $id) checked @endif
                                            data-userId = "{{ $data->id }}" value="{{ $id }}"
                                    data-url="{{ action('Admin\UserCompetitionsControllers@addComp') }}"-->
                                </td>

                                <td> {{ $data->levelName }} </td>

                                <td> {{ $data->User->name }} </td>



                                <td> {{ $data->User->phone }} </td>

                                @if($id == 3)
                                <td>
                                    <a href="{{ action('Admin\UserCompetitionsControllers@completeUserData' ,  $data->id) }}" class="btn sbold blue ">
                                        <i class="fa fa-check"></i> </a>

                                </td>
                                @endif


                                <td>
                                    <a href="{{ action('Admin\CompetitorsController@edit' ,  $data->userId) }}" class="btn sbold blue ">
                                        <i class="fa fa-eye"></i> </a>

                                </td>

                                <td>
                                    @if($data->active == 0)

                                        <a href="#" data-isActive="1" data-id="{{ $data->id }}" data-url="{{ action('Admin\UserCompetitionsControllers@activeCopm') }}"
                                           class="btn sbold green activeComp">
                                            <i class="fa fa-check"></i> </a>


                                        <a href="#" data-isActive="2" data-id="{{ $data->id }}" data-url="{{ action('Admin\UserCompetitionsControllers@activeCopm') }}"
                                           class="btn sbold red activeComp">
                                            <i class="fa fa-times"></i> </a>

                                    @elseif($data->active == 1)
                                        <a href="#" data-isActive="2" data-id="{{ $data->id }}" data-url="{{ action('Admin\UserCompetitionsControllers@activeCopm') }}"
                                           class="btn sbold red activeComp">
                                            <i class="fa fa-times"></i> </a>


                                    @elseif($data->active == 2)
                                    <a href="#" data-isActive="1" data-id="{{ $data->id }}" data-url="{{ action('Admin\UserCompetitionsControllers@activeCopm') }}"
                                       class="btn sbold green activeComp">
                                        <i class="fa fa-check"></i> </a>
                                        @endif



                                </td>



                            </tr>
                            <?php $i++; ?>
                        @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>


    </div>



@endsection




