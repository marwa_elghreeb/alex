@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET-->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-plus"></i> @lang('admin.Edit item')
            </div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" enctype="multipart/form-data" method="post"
                  action="{{ action('Admin\ExaminerController@update' , $editData->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">


                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="tabbable-custom ">


                        <ul class="nav nav-tabs ">


                            @foreach($allLang as $data)
                                <li @if($data->symbol == 'ar') class="active" @endif>
                                    <a href="#tab_{{$data->id}}" data-toggle="tab">
                                        <img src="{{ URL ::to ('public/images/'.$data->flag)}}" width="20px;"
                                             height="20px;"/>
                                        {{$data->name}} </a>
                                </li>
                            @endforeach


                        </ul>
                        <div class="tab-content">
                            <?php $i =0 ?>
                            @foreach($allLang as $data)
                                <div class="tab-pane @if($data->symbol == 'ar') active @endif" id="tab_{{$data->id}}">
                                    <div class="form-body">
                                        @if($data->symbol == 'ar')

                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.OrderNo')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="orderNo" value="{{ $editData->orderNo  }}">

                                                </div>
                                            </div>


                                        @endif



                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Name')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="name_{{$data->symbol}}" value="{{ $editData->name }}">

                                                </div>
                                            </div>


                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Jobs')

                                                </label>
                                                <div class="col-md-9">

                                                    <input type="text" class="form-control"  name="title_{{$data->symbol}}" value="{{ $editData->title }}">

                                                </div>
                                            </div>


                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  @lang('admin.Active')

                                                </label>
                                                <div class="col-md-9">
                                                    <select class="form-control select2" name="active">
                                                        <option value="1" @if($editData->active == 1 ) selected @endif>@lang('admin.isActive')</option>
                                                        <option value="2" @if($editData->active == 2 ) selected @endif>@lang('admin.notActive')</option>
                                                    </select>
                                                </div>
                                            </div>


                                            <div class="form-group last">
                                                <label class="control-label col-md-3">  نوع المسابقه

                                                </label>
                                                <div class="col-md-9">
                                                    <select class="form-control select2" name="competitionId">
                                                        @foreach($allCat as $cat)
                                                            <option value="{{ $cat->id }}"
                                                                    @if($cat->id == $editData->competitionId ) selected @endif>{{ $cat->name }}</option>
                                                        @endforeach

                                                    </select>
                                                </div>
                                            </div>



                                    </div>

                                </div>
                            @endforeach

                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-8">
                                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                                        <button type="button" class="btn default"
                                                onclick="window.history.back()">@lang('admin.Cancel')
                                        </button>
                                    </div>
                                </div>
                            </div>
            </form>

        </div>


    </div>


    </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->
    </div>


@endsection




