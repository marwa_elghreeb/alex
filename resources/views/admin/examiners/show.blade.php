@extends('layouts.app')
@section('content')


    <div class="row">

        <div class="col-md-12">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="icon-users font-green"></i>
                        <span class="caption-subject bold font-green uppercase"> لجنه الممتحنين</span>

                    </div>

                    <div class="actions">
                        <div class="btn-group">
                            <a href="{{ action('Admin\ExaminerController@create') }}" class="btn sbold green ">
                                <i class="fa fa-plus"></i>  @lang('admin.Add new item')</a>
                        </div>
                    </div>

                </div>
                <div class="portlet-body">

                    @if(count($allData) > 0)
                        <div class="timeline">


                        @foreach($allData as $data)
                            <!-- TIMELINE ITEM -->
                                <div class="timeline-item">
                                    <div class="timeline-badge">
                                        <div class="timeline-icon">
                                            <i class="icon-user-following font-green-haze"></i>
                                        </div>
                                    </div>
                                    <div class="timeline-body">
                                        <div class="timeline-body-arrow"></div>
                                        <div class="timeline-body-head">
                                            <div class="timeline-body-head-caption">
                                                <span class="timeline-body-alerttitle font-red-intense">{{ $data->name }}</span>
                                            </div>
                                            <div class="timeline-body-head-actions">


                                            </div>
                                        </div>
                                        <div class="timeline-body-content">
                                            <span class="font-grey-cascade"> {{ $data->title  }}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END TIMELINE ITEM -->


                        </div>

                        @endforeach

                    @else

                        <div class="alert alert-info"> لا يوجد ممتحنين</div>
                    @endif
                </div>
            </div>
        </div>


    </div>



@endsection




