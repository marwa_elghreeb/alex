@extends('layouts.app')
@section('content')

    <!-- BEGIN SAMPLE FORM PORTLET -->
    <div class="portlet box  green ">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-pencil"></i>@lang('admin.Edit item')</div>
            <div class="tools"></div>
        </div>
        <div class="portlet-body form">
            <form class="form-horizontal" role="form" method="post" enctype="multipart/form-data"
                  action="{{ action('Admin\FatwaController@update' ,  $editData->id) }}">
                {{ csrf_field() }}
                {{ method_field('PATCH') }}
                <div class="form-body">

                    <ul>
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger"> {{ $error }}</div>
                        @endforeach
                    </ul>


                    <div class="form-group">
                        <label class="col-md-3 control-label"> عرض فى الصفحه الرئيسيه </label>
                        <div class="col-md-9">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="inHome" value="1"
                                           @if($editData->inHome == 1) checked @endif> نعم
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="inHome" value="2"
                                           @if($editData->inHome == 2) checked @endif> لا
                                    <span></span>
                                </label>

                            </div>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">عنوان الفتوى</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="question" value="{{ $editData->question }}">
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="col-md-3 control-label">تفاصيل الفتوى</label>
                        <div class="col-md-9">
                            <table class="table table-bordered table-hover">
                                <tr>
                                    <th width="20%">التاريخ</th>
                                    <th width="20%"> اسم المستخدم</th>
                                    <th> الرد</th>

                                </tr>
                                <tbody>


                                @foreach($details as $del)
                                    <tr>
                                        <td><?php echo date("Y-m-d", strtotime($del->created_at)); ?></td>
                                        <td>
                                            @if($del->userId == 0)
                                                الاداره
                                            @else

                                            @endif


                                        </td>
                                        <td><textarea class="form-control" rows="6" name="replay">{{ $del->replay }}</textarea></td>

                                    </tr>
                                @endforeach


                                </tbody>
                            </table>


                        </div>
                    </div>


                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" class="btn green">@lang('admin.Save') </button>
                            <button type="button" class="btn default"
                                    onclick="window.history.back()">@lang('admin.Cancel')</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- END SAMPLE FORM PORTLET-->

@endsection




