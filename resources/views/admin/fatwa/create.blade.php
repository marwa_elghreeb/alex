@extends('layouts.app')
@section('content')

<!-- BEGIN SAMPLE FORM PORTLET-->
<div class="portlet box  green ">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-plus"></i>   @lang('admin.Add new item') </div>
        <div class="tools"></div>
    </div>
    <div class="portlet-body form">
        <form class="form-horizontal"
              role="form" enctype="multipart/form-data" method="post" action="{{ action('Admin\FatwaController@store') }}">
            {{ csrf_field() }}
            <div class="form-body">


                <ul>
                    @foreach ($errors->all() as $error)
                         <div class="alert alert-danger"> {{ $error }}</div>
                    @endforeach
                </ul>

                <div class="form-group">
                    <label class="col-md-3 control-label"> عرض فى الصفحه الرئيسيه </label>
                    <div class="col-md-9">
                        <div class="mt-radio-inline">
                            <label class="mt-radio">
                                <input type="radio" name="inHome"  value="1" > نعم
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" name="inHome"  value="2" checked> لا
                                <span></span>
                            </label>

                        </div>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">عنوان الفتوى</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" name="question"   value="{{ old('question') }}"> </div>
                </div>




                <div class="form-group">
                    <label class="col-md-3 control-label">تفاصيل الفتوى</label>
                    <div class="col-md-9">

                       <textarea class="form-control" rows="6" name="replay">

                                        </textarea>
                        </div>
                </div>







            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" class="btn green">@lang('admin.Save') </button>
                        <button type="reset" class="btn default" onclick="window.history.back()">@lang('admin.Cancel')</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- END SAMPLE FORM PORTLET-->

@endsection




