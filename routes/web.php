<?php

/*==Admin Linkes===================================================*/
Route::get('/admin', function () {
    if (\Illuminate\Support\Facades\Auth::check()) {
        return redirect('/admin/home');
    } else {
        return redirect('/login');
    }
});
/*Auth=======*/
Auth::routes();
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::group(['middleware' => ['auth', 'locale'], 'prefix' => 'admin', 'namespace' => 'Admin'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/changelang/{locale}', 'SettingController@changeLang');


    /*Menu=======*/
    Route::resource('menu', 'MenuController');
    /*Language=======*/
    Route::resource('language', 'LanguageController');

    /*General===================================*/
    Route::group(['middleware' => 'permissionMenu'], function () {

        /*User Groups=======*/
        Route::resource('usergroups', 'UsergroupsController');
        /*Admin=======*/
        Route::resource('admin', 'AdminController');
        /*User Relation=======*/
        Route::resource('userRelations', 'UserRelationsController');
        /*Setting=======*/
        Route::any('/setting', 'SettingController@index');
        Route::any('/setting/update/{id}', 'SettingController@update');
        Route::any('/setting/delItem', 'SettingController@delItem');
        /*Slider=======*/
        Route::resource('slider', 'SliderController');
        /*Pages=======*/
        Route::resource('page', 'PagesController');
        /*Blog=======*/
        Route::resource('news', 'NewsController');
        /*userNotes======*/
        Route::resource('userNotes', 'UserNotesController');
        /*fatwa=======*/
        Route::resource('fatwa', 'FatwaController');
        /*== competitions=============================================*/
        Route::resource('category', 'CateoryController');
        /*Main Competiotion========*/
        Route::resource('competitions', 'CompetitionsController');
        Route::post('/getLevel', 'CompetitionsController@getLevel');
        /*CompetitionsLevel=======*/
        Route::resource('competitionsLevel', 'CompetitionsLevelController');
        /*CompetitionsCondition=======*/
        Route::resource('competitionsCondition', 'CompetitionsConditionController');
        /*CompetitionsBand=======*/
        Route::resource('competitionsBand', 'CompetitionsBandController');
        /*competitors=======*/
        Route::resource('competitors', 'CompetitorsController');
        Route::any('activeCopm', 'UserCompetitionsControllers@activeCopm');
        /*CompetitionsUser=======*/
        Route::resource('userCompetitions', 'UserCompetitionsControllers');
        Route::post('addComp', 'UserCompetitionsControllers@addComp');
        Route::get('completeUserData/{id}', 'UserCompetitionsControllers@completeUserData');
        /*examiners=====================================================*/
        Route::resource('examiners', 'ExaminerController');
        /*exams=======*/
        Route::resource('exams', 'ExamsController');
        /*exams Degree=======*/
        Route::resource('examsDegree', 'ExamDegreeController');
        /*exams Degree=======*/
        Route::resource('examsDates', 'ExamesDatesController');
        /*Meals===================================================*/
        Route::resource('mealsMain', 'MealsMainController');
        /*Items================================================*/
        Route::resource('items', 'ItemController');
        Route::post('/getQuantity', 'ItemController@getQuantity');
        /*ItemsAdd===========*/
        Route::resource('itemsAdd', 'ItemAddController');
        /*ItemsRequest===========*/
        Route::resource('itemsRequest', 'ItemRequestController');
        /*MessageUser==============================================*/
        Route::resource('messageUsers', 'MessageUsersController');
        /* Message ========*/
        Route::get('message', 'MessageController@contactUs');
        Route::get('subscribe', 'MessageController@subscribe');
        Route::get('deletesubscribe/{id}', 'MessageController@deletesubscribe');
        /*City==================================================*/
        Route::resource('city', 'CityController');
        Route::post('/getCity', 'CityController@getCity');
        Route::post('/getSubCity', 'CityController@getSubCity');
        /*CityState=======*/
        Route::resource('cityState', 'CityStateController');
        Route::get('/delCityState/{id}', 'CityStateController@delCityState');
        /*CityQuarter=======*/
        Route::resource('cityQuarter', 'CityQuarterController');
        /*Section==============================================*/
        Route::resource('sections', 'SectionsControllers');
        /*SectionItems=============*/
        Route::resource('sectionsItems', 'SectionItemsController');
    });

});


/*====Site Link==================================================*/
Route::group(['namespace' => 'Site'], function () {
    //All sit links
    Route::get('/', 'IndexController@index');
    Route::get('/msg', 'IndexController@msg');
    //users
    Route::group(['prefix' => 'user',], function () {
        Route::get('/login', 'UserController@login');
        Route::post('/register', 'UserController@signUpForm');
        Route::post('/loginForm', 'UserController@signForm');
        Route::group(['middleware' => 'userWebLogin'], function () {
            Route::get('/profile', 'UserController@profile');
            Route::post('/checkUserIsValid', 'UserController@checkUserIsValid');
            Route::get('/applayCompetitions/{id}', 'UserController@applayCompetitions');
            Route::post('/applayCompetitionsForm', 'UserController@applayCompetitionsForm');
            Route::get('/userLogout', 'UserController@userLogout');
        });
    });
    //Competitions
    Route::group(['middleware' => 'userWebLogin', 'prefix' => 'competitions',], function () {
        Route::get('/all/{id}', 'CompetitionsController@index');
        Route::get('/conditions/{id}', 'CompetitionsController@conditions');
        Route::get('/awards/{id}', 'CompetitionsController@awards');
        Route::get('/bands/{id}', 'CompetitionsController@bands');
    });
});
