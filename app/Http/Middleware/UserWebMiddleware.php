<?php

namespace App\Http\Middleware;

use Closure;

class UserWebMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(empty(session('userId')))
        {
            return redirect('user/login');
        }
        else{
            return $next($request);
        }


    }
}
