<?php

namespace App\Http\Middleware;

use Closure;
use App\User;
use App\Menu;
use App\UserRole;
use App\UserController;
use Auth;
use \Illuminate\Support\Facades\Request;

class PermissionMenuMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $userRoles = Auth::user()->roleId;

        $userMenu = UserRole::where(['roleId' => $userRoles])->get();
        $link =  'admin/'.$request->segment(2);  //$request->path();

       // dd($link);
      //  die();
        $mData = Menu::where('link', $link)->first();

        if ($userMenu->contains('menuId', $mData->id)) {

            return $next($request);

        } else {


            return redirect('admin/perm');
        }

    }
}
