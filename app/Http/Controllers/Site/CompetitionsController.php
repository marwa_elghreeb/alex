<?php

namespace App\Http\Controllers\Site;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Cateory;
use App\CompetitionsConditions;
use App\CompetitionsLevel;
use App\Awards;
use App\Competitions;
use App\CompetitionBand;
use App\CompetitionsReaders;
use App\UserRelation;

class CompetitionsController extends Controller
{


    public function index($id)
    {
        $catData = Cateory::find($id);
        $nameArr = json_decode($catData->name , true);
        $catName = $nameArr[Lang::getLocale()];

        $allComp = Competitions::where('catId' , $id)->orderBy('id' , 'DESC')->get();
        foreach ($allComp as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('site.competitions.index')->with([ 'catName' => $catName ,'allComp' => $allComp  ]);
    }



    public function conditions($id)
    {
        $catData = Cateory::find($id);
        $nameArr = json_decode($catData->name , true);
        $catName = $nameArr[Lang::getLocale()];

        $conditions = CompetitionsConditions::where(['competitionId' => $id ])->orderBy('orderNo' , 'ASC')->get();
        return view('site.competitions.conditions')->with([ 'conditions' => $conditions , 'catName' => $catName ]);
    }



    public function awards($id)
    {
        $catData = Cateory::find($id);
        $nameArr = json_decode($catData->name , true);
        $catName = $nameArr[Lang::getLocale()];
        $relations = UserRelation::all();

        $levels = CompetitionsLevel::where(['typeId' => $id ])->orderBy('orderNo' , 'ASC')->get();
        foreach ($levels as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

            $awards = Awards::where('levelId' , $data->id)->get();
            $data->awards = $awards;

            $readers = CompetitionsReaders::where('levelId' , $data->id)->get();
            $data->readers = $readers;

        }


        return view('site.competitions.awards')->with([ 'levels' => $levels , 'catName' => $catName ,
            'mainId' => $id ,   'relations' => $relations , 'catData' => $catData ,]);
    }



    public function bands($id)
    {
        $catData = Cateory::find($id);
        $nameArr = json_decode($catData->name , true);
        $catName = $nameArr[Lang::getLocale()];
         //
        $conditions = CompetitionBand::where(['competitionId' => $id ])->orderBy('orderNo' , 'ASC')->get();
        return view('site.competitions.bands')->with([ 'conditions' => $conditions , 'catName' => $catName ]);
    }












}
