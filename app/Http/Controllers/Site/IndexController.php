<?php

namespace App\Http\Controllers\Site;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Slider;
use App\News;
use App\Fatwa;


class IndexController extends Controller
{

    public function __construct()
    {
    }

    public function changeLang($lang)
    {
        Session(['locale' => $lang]);
        return redirect()->back();
    }


    public function index()
    {
        $sliders = slider::all();
        $news = News::take(5)->get();
        foreach ($news as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

            $contentsArr = json_decode($data->content , true);
            $data->content = $contentsArr[Lang::getLocale()];

        }
        $fatwa = Fatwa::where('inHome' , 1)->take(5)->orderBy('id' , 'DESC')->get();
        return view('site.web.index')->with([ 'sliders' => $sliders , 'news' => $news , 'fatwa' => $fatwa]);
    }




    public function msg()
    {

        return view('site.web.msg')->with([]);
    }







}
