<?php

namespace App\Http\Controllers\Site;


use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\UserWeb;
use Session;
use App\City;
use App\CityState;
use App\Competitors;
use App\UserCompetitions;
use App\CompetitionsLevel;
use App\Competitions;
use App\Notifications;
use App\ExamesDate;
use App\ExamesHours;
use App\Cateory;



class UserController extends Controller
{
    public function __construct()
    {
        $userData = UserWeb::find(session('userId'));
        view()->share([
            'userData' => $userData,

        ]);
    }


    /*Login & Registration =======*/
    public function login()
    {
        session(['link' => url()->previous()]);
        return view('site.user.login');
    }

    public function signForm(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $userData = UserWeb::where(['email' => $request->email, 'password' => md5($request->password)])->first();
        if (count($userData) > 0) {
            //creat user session
            session(['userId' => $userData->id,
                'userName' => $userData->name]);


           return redirect()->action('Site\CompetitionsController@index'  , 1);
        } else {
            return redirect()->action('Site\UserController@login');
        }
    }


    public function signUp()
    {
        return view('site.signUp');
    }

    public function signUpForm(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|unique:users-web|email',
            'phone' => 'required|unique:users-web|numeric|min:11',
            'password' => 'required|min:6|confirmed',
        ]);
        //Insert new user
        $insert = new UserWeb();
        $insert->name = $request->input('name');
        $insert->email = $request->input('email');
        $insert->password = md5($request->input('password'));
        $insert->phone = $request->input('phone');
        $insert->save();
        //insert in competitors
        $details = new Competitors();
        $details->userId = $insert->id;
        $details->save();

        //creat user session
        session(['userId' => $insert->id, 'userName' => $request->input('username')]);
        //
        return redirect()->action('Site\UserController@profile');
    }


    /*Profile Details==============*/
    public function profile()
    {
        $userData = UserWeb::find(session('userId'));

        $details =  Competitors::where('userId' , session('userId'))->first();
        $notification = Notifications::where('userId', session('userId'))->orderBy('id', 'DESC')->get();
        $exams = ExamesHours::where('userId', session('userId'))->first();
        $code = UserCompetitions::all();
        $mainCity = City::all();
        $city = CityState::all();
        return view('site.user.profile')->with(['userData' => $userData, 'details' => $details,'notification' => $notification
            , 'exams' => $exams, 'code' => $code, 'mainCity' => $mainCity, 'city' => $city,]);
        $notification = Notifications::where('userId' , session('userId'))->orderBy('id' , 'DESC')->get();
        return view('site.user.profile')->with([ 'userData' =>  $userData , 'notification' => $notification]);
    }

    /*User Competitions==============*/
    public function applayCompetitions($id)
    {
        $userData = UserWeb::find(session('userId'));
        $city = CityState::where('cityId', 3)->get();
        $code = UserCompetitions::all();

        return view('site.user.applay')->with(['userData' => $userData, 'levelId' => $id, 'city' => $city, 'code' => $code]);

    }

    public function applayCompetitionsForm(Request $request)
    {

        $this->validate($request, [
            'code' => 'numeric',
           // 'nationalId' => 'required|unique:competitors|numeric|min:0|max:14',
            'area' => 'required',
            'address' => 'required',
            'phone1' => 'required|numeric',
            'email' => 'required|email',
            'age' => 'required',
            'job' => 'required',
            'education' => 'required',
            'gender' => 'required|in:ذكر,انثى',
        ]);
        //insert Competitors data
        $code = UserCompetitions::all();

        $checkData = Competitors::where('userId' , session('userId'))->first();

        if ($request->hasFile('imageName')) {
            //
            $imageName = time() . '.' . $request->file('imageName')->getClientOriginalExtension();
            $request->file('imageName')->move(public_path('images'), $imageName);
        } else {
            $imageName = '';
        }
        $data =  Competitors::find($checkData->id);
        $data->userId = session('userId');
        $data->imageName = $imageName;
        $data->code = count($code) + 1;
        $data->userName = $request->input('name');
        $data->nationalId = $request->input('nationalId');
        $data->government = $request->input('government');
        $data->city = $request->input('city');
        $data->area = $request->input('area');
        $data->address = $request->input('address');
        $data->phone1 = $request->input('phone1');
        $data->phone2 = $request->input('phone2');
        $data->phoneHome = $request->input('phoneHome');
        $data->email = $request->input('email');
        $data->education = $request->input('education');
        $data->job = $request->input('job');
        $data->gender = $request->input('gender');
        $data->age = date("Y-m-d" , strtotime($request->input('age')));
        $data->shareBefore = $request->input('shareBefore');
        $data->save();


        return redirect()->action('Site\UserController@profile');
    }

    public function checkUserIsValid(Request $request)
    {
       $userId = $request->userId;
       $userData = Competitors::where('userId' , $userId)->first();
       //Comp Data
        $compData = Cateory::find($request->compId);
        $govern= explode(',' ,$compData->city);
        //Calc competition age
        $age = date_diff(date_create($userData->age), date_create('now'))->y;

        if (in_array($userData->government, $govern) == false)
        {
            //allowed to join competition
            return 'عفوا المسابقه لاتشمل محافظتك الحاليه';


        }elseif ($age > $compData->maxAge || $age < $compData->minAge)
        {
            //not allowed
            return 'غير مسموح للتسجيل فى المسابقه لعد استيفاء شرط العمر';

        } else{
            //assigin user To Competitors
             $levelData = CompetitionsLevel::find($request->levelId);
             $compData = Competitions::where('catId' , $levelData->typeId)->orderBy('id', 'DESC')->first();
             //
             $userComp = new UserCompetitions();
             $userComp->mainCompetitionsId = $levelData->typeId;
             $userComp->competitionsId = $compData->id;
             $userComp->levelId = $request->levelId;
             $userComp->userId = $userId;
             $userComp->active = 0;
             $userComp->save();
            //Assign Exam Date to Competitors
             $allLevelExams = ExamesDate::where(['levelId' => $request->levelId , 'type' => $request->input('gender') ])->get();
             foreach ($allLevelExams as $exam)
             {
                 $examDate = ExamesHours::where([
                     'examId' => $exam->id,
                     'type' => $userData->gender,
                     'state' => 0 ,
                 ])->inRandomOrder()->first();

                 //update user exam
                 ExamesHours::where(['id' => $examDate->id])->update(['state' =>1 , 'userId' => session('userId')]);
             }
            return 'تم اشتراكك فى المسابقه بنجاح ';
        }

    }


        /*Logout==============*/
    public function userLogout()
    {
        Session::forget('userId');
        Session::forget('userName');
        return redirect()->action('Site\IndexController@index');
    }

}

