<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Language;
use App\Items;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = Items::all();
        foreach ($allData as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.items.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        return view('admin.items.create')->with([ 'allLang' => $allLang , ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
                'quantity'  => 'required|numeric',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //Insert
        $insert = new Items();
        $insert->name = json_encode($names);
        $insert->quantity = $request->input('quantity' );
        $insert->save();
        //
        return redirect()->action('Admin\ItemController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $editData = Items::find($id);
        $nameArr = json_decode($editData->name , true);
        $editData->name =  $nameArr[Lang::getLocale()];


        return view('admin.items.edit')->with(['editData'=> $editData ,  'allLang' => $allLang ,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
                'quantity'  => 'required|numeric',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //Insert
        $insert =  Items::find($id);
        $insert->name = json_encode($names);
        $insert->quantity = $request->input('quantity' );
        $insert->save();
        //
        return redirect()->action('Admin\ItemController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Items::where('id' , $id)->delete();
        return redirect()->action('Admin\ItemController@index');
    }


    public function getQuantity(Request $request)
    {
        $itemData = Items::find($request->id);
        echo $itemData->quantity;
    }

}
