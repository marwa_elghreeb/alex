<?php

namespace App\Http\Controllers\Admin;

use App\CompetitionsLevel;
use App\Exams;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use DB;
use App\Language;
use App\Cateory;
use App\UserCompetitions;
use App\City;


class CateoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Cateory::all();
        foreach ($category as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

            //
            $chek = UserCompetitions::where(['mainCompetitionsId' => $data->id])->get();
            $data->countUser = count($chek);
        }
        return view('admin.category.index')->with('category', $category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $allLang = Language::all();
        $allCity = City::all();
        return view('admin.category.create')->with(['allLang' => $allLang, 'allCity' => $allCity,]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if (Auth::user()) {
            if ($request->hasFile('imageName')) {
                $files = $request->file('imageName');
                $imgArr = array();
                foreach ($files as $file) {
                    $filename = uniqid() . '_' . $file->getClientOriginalName();
                    $file->move(public_path('/images/'), $filename);
                    $imgArr[] = $filename;
                }
                $img = implode(',', $imgArr);
            } else {
                $img = '';
            }


            if ($request->hasFile('video')) {
                $video = $request->file('video');
                $videoArr = array();
                foreach ($video as $file) {
                    $videoname = uniqid() . '_' . $file->getClientOriginalName();
                    $file->move(public_path('/images/'), $videoname);
                    $videoArr[] = $videoname;
                }

                $vArr = implode(',', $videoArr);
            } else {
                $vArr = '';
            }


            $allLang = Language::all();
            foreach ($allLang as $data) {
                $names[$data->symbol] = $request->input('name_' . $data->symbol);
                $tittles[$data->symbol] = $request->input('tittle_' . $data->symbol);
                $contents[$data->symbol] = $request->input('content_' . $data->symbol);
            }
            $nameArr = json_encode($names);
            $tittleArr = json_encode($tittles);
            $contentsArr = json_encode($contents);
            //Insert
            $main = new Cateory();
            $main->imageName = $img;
            $main->video = $vArr;
            $main->name = $nameArr;
            $main->tittle = $tittleArr;
            $main->content = $contentsArr;
            $main->inHome = $request->inHome;
            $main->save();
            //
            return redirect()->action('Admin\CateoryController@index');
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Show levels and Exams
        $compLevel = CompetitionsLevel::where('typeId', $id)->get();
        foreach ($compLevel as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];


            $exams = Exams::where('levelId', $data->id)->get();
            $data->exams = $exams;

        }
        return view('admin.category.show')->with(['compLevel' => $compLevel]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Auth::user()) {
            $allLang = Language::all();
            $main = Cateory::find($id);
            $nameArr = json_decode($main->name, true);
            $tittleArr = json_decode($main->tittle, true);
            $contentsArr = json_decode($main->content, true);

            $imgArr = explode(',', $main->imageName);
            $videoArr = explode(',', $main->video);

            //  var_dump($tittleArr);
            $allCity = City::all();
            $city = explode(',', $main->city);
            //  dd($nameArr);

            return view('admin.category.edit')->with(['main' => $main, 'imgArr' => $imgArr, 'videoArr' => $videoArr,
                'nameArr' => $nameArr, 'tittleArr' => $tittleArr, 'contentsArr' => $contentsArr, 'allLang' => $allLang,
                'allCity' => $allCity, 'catCity' => $city]);
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        $this->validate($request, [
            'minAge' => 'required|numeric',
            'maxAge' => 'required|numeric',
            'name_ar' => 'required',
            'city' => 'required',
        ]);

        $allLang = Language::all();
        foreach ($allLang as $data) {
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
            $tittles[$data->symbol] = $request->input('tittle_' . $data->symbol);
            $contents[$data->symbol] = $request->input('content_' . $data->symbol);
        }
        $nameArr = json_encode($names);
        $tittleArr = json_encode($tittles);
        $contentsArr = json_encode($contents);
        //Insert
        $main = Cateory::find($id);
        if ($request->hasFile('imageName')) {
            $files = $request->file('imageName');
            $imgArr = array();
            foreach ($files as $file) {
                $filename = uniqid() . '_' . $file->getClientOriginalName();
                $file->move(public_path('/images/'), $filename);
                $imgArr[] = $filename;
            }
            $imgArr = implode(',', $imgArr);

        } else {
            $imgArr = $main->imageName;
        }


        if ($request->hasFile('video')) {
            $video = $request->file('video');
            $videoArr = array();
            foreach ($video as $file) {
                $videoname = uniqid() . '_' . $file->getClientOriginalName();
                $file->move(public_path('/images/'), $videoname);
                $videoArr[] = $videoname;
            }
            $videoArr = implode(',', $videoArr);
        } else {
            $videoArr = $main->video;
        }
        $main->imageName = $imgArr;
        $main->video = $videoArr;
        $main->name = $nameArr;
        $main->tittle = $tittleArr;
        $main->content = $contentsArr;
        $main->inHome = $request->inHome;
        $main->city = implode(',', $request->input('city'));
        $main->minAge = $request->input('minAge');
        $main->maxAge = $request->input('maxAge');
        $main->save();
        //
        return redirect()->action('Admin\CateoryController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id)
    {
        DB::table('cateories')->where('id', '=', $id)->delete();
        return redirect()->action('Admin\CateoryController@index');
    }
}
