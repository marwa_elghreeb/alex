<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\Menu;
use App\UserRole;
use App\UserController;
use App\Cateory;
use App\Role;

class UsergroupsController extends Controller
{
    public function index()
    {

        $allData = Role::all();
        return view('admin.group.index')->with('lang', $allData);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $allMenu = Menu::where('parentId',  0)->get();
        $groupMenu = UserRole::where('roleId', '=', 0)->get();
        return view('admin.group.create')->with(['allMenu' => $allMenu, 'groupMenu' => $groupMenu,]);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, [
            'name' => 'required',
        ]);
        $insert = new Role();
        $insert->name = $request->input('name');

        $insert->save();

        //user menu
        $menu = $request->input('menu');
        if (!empty($menu) && isset($menu)) {
            foreach ($menu as $m) {
                $insert_m = new UserRole();
                $insert_m->roleId = $insert->id;
                $insert_m->menuId = $m;

                $insert_m->save();
            }
        }




        return redirect()->action('Admin\UsergroupsController@index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = Role::find($id);
        //
        $allMenu = Menu::where('parentId',  0)->get();
        $groupMenu = UserRole::where('roleId', '=', $id)->get();
        // dd($groupMenu);
        //

        return view('admin.group.edit')->with(['editData' => $editData, 'allMenu' => $allMenu,
            'groupMenu' => $groupMenu, ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'name' => 'required',
        ]);

        $update = Role::find($id);
        $update->name = $request->input('name');

        $update->save();


        //
        $menu = $request->input('menu');
        if (!empty($menu) && isset($menu)) {
            UserRole::where('roleId', $id)->delete();
            foreach ($menu as $m) {
                $insert_m = new UserRole();
                $insert_m->roleId = $id;
                $insert_m->menuId = $m;

                $insert_m->save();
            }
        }



        return redirect()->action('Admin\UsergroupsController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $checkUsers = User::where('roleId' , $id)->get();
        if(count($checkUsers) > 0)
        {
            return view('error.permission');
        }
        else{
            DB::table('roles')->where('id', '=', $id)->delete();
            return redirect()->action('Admin\UsergroupsController@index');
        }

    }


}
