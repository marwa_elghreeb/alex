<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Language;
use App\Examiners;
use App\Cateory;

class ExaminerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allData = Examiners::all();
        foreach ($allData as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.examiners.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        $allCat = Cateory::all();
        foreach ($allCat as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.examiners.create')->with([ 'allLang' => $allLang ,
            'allCat' => $allCat , ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
                'orderNo' => 'required|numeric',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
            $title[$data->symbol] = $request->input('title_' . $data->symbol);
        }
        //Insert
        $insert = new Examiners();
        $insert->name = json_encode($names);
        $insert->title = json_encode($title);
        $insert->orderNo = $request->input('orderNo' );
        $insert->active = $request->input('active' );
        $insert->competitionId = $request->input('competitionId' );
        $insert->save();
        //
        return redirect()->action('Admin\ExaminerController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $allData = Examiners::where('competitionId' , $id)->get();
        foreach ($allData as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name =  $nameArr[Lang::getLocale()];
            //
            $titleArr = json_decode($data->title , true);
            $data->title =  $titleArr[Lang::getLocale()];

        }
        return view('admin.examiners.show')->with([ 'allData' => $allData ,]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $editData = Examiners::find($id);
        $nameArr = json_decode($editData->name , true);
        $editData->name =  $nameArr[Lang::getLocale()];
        $titleArr = json_decode($editData->title , true);
        $editData->title =  $titleArr[Lang::getLocale()];
        $allCat = Cateory::all();
        foreach ($allCat as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }

        return view('admin.examiners.edit')->with(['editData'=> $editData ,  'allLang' => $allLang ,
            'allCat' => $allCat ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
            $title[$data->symbol] = $request->input('title_' . $data->symbol);
        }
        //update
        $update = Examiners::find($id);
        $update->name = json_encode($names);
        $update->title = json_encode($title);
        $update->orderNo = $request->input('orderNo' );
        $update->active = $request->input('active' );
        $update->competitionId = $request->input('competitionId' );
        $update->save();
        //
        return redirect()->action('Admin\ExaminerController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Examiners::where('id' , $id)->delete();
        return redirect()->action('Admin\ExaminerController@index');
    }
}
