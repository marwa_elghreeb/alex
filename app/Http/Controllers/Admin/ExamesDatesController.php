<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Language;
use App\Cateory;
use App\CompetitionsLevel;
use App\ExamesDate;
use App\ExamesHours;
use DateTime;
use DatePeriod;
use DateInterval;

class ExamesDatesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = ExamesDate::all();
        foreach ($allData as $data) {
            $nameArr = json_decode($data->Competitions->name, true);
            $data->nameCom = $nameArr[Lang::getLocale()];


            $nameArr1 = json_decode($data->Level->name, true);
            $data->nameLevel = $nameArr1[Lang::getLocale()];

        }
        return view('admin.examDates.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        $allType = Cateory::all();
        foreach ($allType as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.examDates.create')->with(['allLang' => $allLang, 'allType' => $allType]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $this->validate($request, [
                'dayEx'  => 'required',
                'from'  => 'required',
                'to'  => 'required',
                'limit'  => 'required',
            ]);
        //Insert
        $insert = new ExamesDate();
        $insert->competitionsId = $request->input('competitionsId');
        $insert->levelId = $request->input('levelId');
        $insert->day	 = $request->input('dayEx');
        $insert->from = $request->input('from');
        $insert->to = $request->input('to');
        $insert->limit = $request->input('limit');
        $insert->type = $request->input('type');
        $insert->period = $request->input('period');
        $insert->break = $request->input('break');
        $insert->save();

        //Add Exam Hours
        $from = $request->input('from');
        $to  = $request->input('to');
        $examDate = $request->input('period') * 60;
        $break = $request->input('break');
        $addTime = $examDate + $break;
        $period = new DatePeriod(new DateTime($from),new DateInterval('PT'.$addTime.'M'), new DateTime($to) );
        foreach ($period as $date) {
           // echo $date->format("H:i\n");
            //Insert in DB
            $sub = new ExamesHours();
            $sub->examId = $insert->id;
            $sub->day = $request->input('dayEx');
            $sub->type = $request->input('type');
            $sub->from = $date->format("H:i\n");
            $sub->to = date('H:i',strtotime($date->format("H:i\n").'+ '.$examDate.' minutes'));
            $sub->state = 0;
            $sub->save();

        }

        return redirect()->action('Admin\ExamesDatesController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $editData = ExamesDate::find($id);
        $allType = Cateory::all();
        foreach ($allType as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }

        $allLevel = CompetitionsLevel::all();
        foreach ($allLevel as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }

        return view('admin.examDates.edit')->with(['editData' => $editData, 'allLang' => $allLang,
            'allType' => $allType, 'allLevel' => $allLevel,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'dayEx'  => 'required',
            'from'  => 'required',
            'to'  => 'required',
            'limit'  => 'required',
        ]);
        //Insert
        $insert =  ExamesDate::find($id);
        $insert->competitionsId = $request->input('competitionsId');
        $insert->levelId = $request->input('levelId');
        $insert->day	 = $request->input('dayEx');
        $insert->from = $request->input('from');
        $insert->to = $request->input('to');
        $insert->limit = $request->input('limit');
        $insert->type = $request->input('type');
        $insert->period = $request->input('period');
        $insert->break = $request->input('break');
        $insert->save();

        ExamesHours::where('examId' , $id)->delete();
        //Add Exam Hours
        $from = $request->input('from');
        $to  = $request->input('to');
        $examDate = $request->input('period') * 60;
        $break = $request->input('break');
        $addTime = $examDate + $break;
        $period = new DatePeriod(new DateTime($from),new DateInterval('PT'.$addTime.'M'), new DateTime($to) );
        foreach ($period as $date) {
            // echo $date->format("H:i\n");
            //Insert in DB
            $sub = new ExamesHours();
            $sub->examId = $insert->id;
            $sub->day = $request->input('dayEx');
            $sub->type = $request->input('type');
            $sub->from = $date->format("H:i\n");
            $sub->to = date('H:i',strtotime($date->format("H:i\n").'+ '.$examDate.' minutes'));
            $sub->state = 0;
            $sub->save();

        }


        return redirect()->action('Admin\ExamesDatesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        ExamesDate::where('id' , $id)->delete();
        return redirect()->action('Admin\ExamesDatesController@index');
    }
}
