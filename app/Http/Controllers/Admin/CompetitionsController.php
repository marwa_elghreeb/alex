<?php

namespace App\Http\Controllers\Admin;

use App\CompetitionsLevel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Cateory;
use App\Language;
use App\Competitions;
use App\CompetitionsConditions;
use App\Awards;
use App\Examiners;


class CompetitionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allData = Competitions::all();
        foreach ($allData as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.competition.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        $allType = Cateory::all();
        foreach ($allType as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        $allExam = Examiners::all();
        foreach ($allExam as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.competition.create')->with([ 'allLang' => $allLang ,
            'allType' => $allType , 'allExam' => $allExam ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
                'dateFrom' => 'required|date',
                'dateTo' => 'required|date',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //Insert
        $insert = new Competitions();
        $insert->name = json_encode($names);;
        $insert->catId = $request->input('catId' );
        $insert->dateFrom = $request->input('dateFrom' );
        $insert->dateTo = $request->input('dateTo' );
        $insert->save();
        //
        return redirect()->action('Admin\CompetitionsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $editData = Competitions::find($id);
        $nameArr = json_decode($editData->name , true);
        $editData->name =  $nameArr[Lang::getLocale()];

        $allType = Cateory::all();
        foreach ($allType as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }

        return view('admin.competition.edit')->with(['editData'=> $editData ,  'allLang' => $allLang ,  'allType' => $allType ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
                'dateFrom' => 'required|date',
                'dateTo' => 'required|date',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //update
        $update = Competitions::find($id);
        $update->name = json_encode($names);;
        $update->catId = $request->input('catId' );
        $update->dateFrom = $request->input('dateFrom' );
        $update->dateTo = $request->input('dateTo' );
        $update->save();
        //
        return redirect()->action('Admin\CompetitionsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Competitions::where('id' , $id)->delete();
        return redirect()->action('Admin\CompetitionsController@index');
    }


    public function getLevel(Request $request)
    {
        $id = $request->id;
        $allLevel = CompetitionsLevel::where('typeId' , $id)->get();
        foreach ($allLevel as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

            //getAwardes
            $awards = Awards::where([ 'levelId' => $data->id ])->get();

            $data->awards = $awards;

        }

        return view('admin.competition.level')->with([ 'allLevel' => $allLevel ]);
    }
}
