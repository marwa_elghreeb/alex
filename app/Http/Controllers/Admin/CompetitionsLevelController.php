<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use DB;
use App\Cateory;
use App\Language;
use App\CompetitionsLevel;
use App\Awards;
use App\CompetitionsReaders;


class CompetitionsLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allData = CompetitionsLevel::all();
        foreach ($allData as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

            $catData = Cateory::find($data->typeId);
            $nameArr1 = json_decode($catData->name, true);
            $data->catName = $nameArr1[Lang::getLocale()];

        }
        return view('admin.level.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        $allType = Cateory::all();
        foreach ($allType as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.level.create')->with(['allLang' => $allLang, 'allType' => $allType]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
                'typeId' => 'required',
                'orderNo' => 'required|numeric',
                'parts' => 'required',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //Insert
        $insert = new CompetitionsLevel();
        $insert->name = json_encode($names);;
        $insert->typeId = $request->input('typeId');
        $insert->orderNo = $request->input('orderNo');
        $insert->parts = $request->input('parts');
        $insert->save();
        //insert Awards
        if(!empty($request->input('name')))
        {
            $count = $request->input('name');
            for ($x = 0; $x < count($count); $x++) {

                if(!empty($request->input('name')[$x])) {
                    $subItem = new Awards();
                    $subItem->typeId = $request->input('typeId');
                    $subItem->levelId = $insert->id;
                    $subItem->name = $request->input('name')[$x];
                    $subItem->title = $request->input('icon')[$x];
                    $subItem->content = $request->input('link')[$x];
                    $subItem->save();
                }
            }

        }else{}


        //insert Readers
        if(!empty($request->input('itemName')))
        {
            $count = $request->input('itemName');
            for ($x = 0; $x < count($count); $x++) {
                if(!empty($request->input('itemName')[$x])) {
                    $subItem = new CompetitionsReaders();
                    $subItem->levelId = $insert->id;
                    $subItem->name = $request->input('itemName')[$x];
                    $subItem->save();
                }
            }

        }

        return redirect()->action('Admin\CompetitionsLevelController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $editData = CompetitionsLevel::find($id);
        $nameArr = json_decode($editData->name, true);
        $editData->name = $nameArr[Lang::getLocale()];

        $allType = Cateory::all();
        foreach ($allType as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }

        $subItem = Awards::where('levelId' , $id )->get();

        $reades = CompetitionsReaders::where('levelId' , $id )->get();

        return view('admin.level.edit')->with(['editData' => $editData, 'allLang' => $allLang,
            'allType' => $allType , 'subItem' => $subItem ,  'reades' => $reades]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
                'typeId' => 'required',
                'orderNo' => 'required|numeric',
                'parts' => 'required',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //update
        $update = CompetitionsLevel::find($id);
        $update->name = json_encode($names);;
        $update->typeId = $request->input('typeId');
        $update->orderNo = $request->input('orderNo');
        $update->parts = $request->input('parts');
        $update->save();
        //
        $count = $request->input('name');
        for ($x = 0; $x < count($count); $x++) {

            if (!empty($request->input('id')[$x])) {


                $subItem  = Awards::find($request->input('id')[$x]);
                $subItem->typeId = $request->input('typeId');
                $subItem->levelId = $id;
                $subItem->name = $request->input('name')[$x];
                $subItem->title = $request->input('icon')[$x];
                $subItem->content = $request->input('link')[$x];
                $subItem->save();
            }
            elseif (empty($request->input('id')[$x])) {

                $subItem = new Awards();
                $subItem->typeId = $request->input('typeId');
                $subItem->levelId = $id;
                $subItem->name = $request->input('name')[$x];
                $subItem->title = $request->input('icon')[$x];
                $subItem->content = $request->input('link')[$x];
                $subItem->save();

            }
        }


        //Readers
        $count = $request->input('itemName');
        for ($x = 0; $x < count($count); $x++) {

            if (!empty($request->input('itemId')[$x])) {
                $subItem  = CompetitionsReaders::find($request->input('itemId')[$x]);
                $subItem->levelId = $id;
                $subItem->name = $request->input('itemName')[$x];
                $subItem->save();
            }
            elseif (empty($request->input('itemId')[$x])) {
                $subItem = new CompetitionsReaders();
                $subItem->levelId = $id;
                $subItem->name = $request->input('itemName')[$x];
                $subItem->save();

            }
        }



        return redirect()->action('Admin\CompetitionsLevelController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        CompetitionsLevel::where('id', $id)->delete();
        return redirect()->action('Admin\CompetitionsLevelController@index');
    }
}
