<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Cateory;
use App\Language;
use App\CompetitionsConditions;

class CompetitionsConditionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allData = Cateory::all();
        foreach ($allData as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.competitionCondition.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $allType = Cateory::all();
        foreach ($allType as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        $editData = CompetitionsConditions::where('competitionId' , $id)->get();
        return view('admin.competitionCondition.edit')->with([ 'allLang' => $allLang ,
            'allType' => $allType , 'catId' => $id , 'editData' => $editData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $itr2 = $request->input('name');
        for ($x = 0; $x < count($itr2); $x++) {

            if (!empty($request->input('id')[$x])) {

                $update  = CompetitionsConditions::find($request->input('id')[$x]);
                $update->competitionId = $id;
                $update->orderNo = $request->input('name')[$x];
                $update->name = $request->input('icon')[$x];
                $update->title = $request->input('link')[$x];
                $update->save();
            }
            elseif (empty($request->input('id')[$x])) {

                $insert = new CompetitionsConditions();
                $insert->competitionId = $id;
                $insert->orderNo = $request->input('name')[$x];
                $insert->name = $request->input('icon')[$x];
                $insert->title = $request->input('link')[$x];
                $insert->save();

            }
        }

        return redirect()->action('Admin\CompetitionsConditionController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
