<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Language;
use App\MealsMain;
use App\MealsType;

class MealsMainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = MealsMain::all();
        foreach ($allData as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.mealsMain.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        return view('admin.mealsMain.create')->with(['allLang' => $allLang]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //Insert
        $insert = new MealsMain();
        $insert->name = json_encode($names);;
        $insert->save();
        //insert Awards
        $count = $request->input('name');
        for ($x = 0; $x < count($count); $x++) {
            $subItem = new MealsType();
            $subItem->mealId = $insert->id;
            $subItem->name = $request->input('name')[$x];
            $subItem->save();
        }
        return redirect()->action('Admin\MealsMainController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $editData = MealsMain::find($id);
        $nameArr = json_decode($editData->name, true);
        $editData->name = $nameArr[Lang::getLocale()];

        $subData = MealsType::where('mealId' , $id)->get();

        return view('admin.mealsMain.edit')->with(['editData' => $editData, 'allLang' => $allLang,
            'subData' => $subData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //Insert
        $insert = MealsMain::find($id);
        $insert->name = json_encode($names);;
        $insert->save();
        //insert
        MealsType::where('mealId' ,$id )->delete();
        $count = $request->input('name');
        for ($x = 0; $x < count($count); $x++) {
            $subItem = new MealsType();
            $subItem->mealId = $insert->id;
            $subItem->name = $request->input('name')[$x];
            $subItem->save();
        }
        return redirect()->action('Admin\MealsMainController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        MealsMain::where('id' , $id)->delete();
        MealsType::where('mealId' ,$id )->delete();
        return redirect()->action('Admin\MealsMainController@index');
    }
}
