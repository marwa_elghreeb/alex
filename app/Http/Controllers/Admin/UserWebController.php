<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use DB;
use Auth;
use App\UserWeb;
use App\Usergroups;
use App\Menu;

class UserWebController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()) {
            //
            $allData = UserWeb::all();
            foreach ($allData as $data)
            {
                if($data->parentId != 0)
                {
                    //member
                    $checkData = UserWeb::where('parentId' , '=' , $data->id)->get();
                    $data->check = count($checkData);

                    $pData = UserWeb::find($data->parentId);
                    $data->parentName = $pData->name;
                }
                else{
                    //root
                    $checkData = UserWeb::where('mainRoot' , '=' , $data->id)->get();
                    $data->check = count($checkData);
                    $data->parentName = '';
                }

            }

            return view('admin.user.index')->with('allData', $allData);
        } else {
            return redirect()->route('login');
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if (Auth::user()) {
            $allData = UserWeb::all();
            $serialNo = count($allData) + 1 ;
            $allGroup = Usergroups::all();

            return view('admin.user.create')->with([ 'serialNo' => $serialNo , 'allData' => $allData ,'allGroup' => $allGroup ,
                'userName' => '']);
        } else {
            return redirect()->route('login');
        }
    }



    public function addRoot()
    {
        //
        if (Auth::user()) {
            $allData = UserWeb::all();
            $serialNo = Helper::randomString();
            $allGroup = Usergroups::all();

            return view('admin.user.addRoot')->with([ 'serialNo' => $serialNo , 'allData' => $allData ,'allGroup' => $allGroup ,
                'userName' => '']);
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if (Auth::user()) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users-web',
                'phone' => 'required|numeric|unique:users-web',
                'password' => 'required',
                'serialNo' => 'required',
                'parentId' => 'required',
            ]);
            $insert =  new UserWeb();
            $insert->name = $request->input('name' );
            $insert->email = $request->input('email' );
            $insert->password = md5($request->input('password' ));
            $insert->phone = $request->input('phone' );
            $insert->serialNo = $request->input('serialNo' );
            $insert->levelNo = $request->input('levelNo' );
            $insert->parentId = $request->input('parentId' );
            $insert->groupId = $request->input('groupId' );
            $insert->mainRoot = $request->input('mainRoot' );
            $insert->addUser = 2;
            $insert->addBy = 0;
            $insert->isActive = 1;
            $insert->save();
            return redirect()->action('Admin\UserWebController@index');
        } else {
            return redirect()->route('login');
        }
    }


    public function storeRoot(Request $request)
    {

        if (Auth::user()) {
            $this->validate($request, [
                'name' => 'required',
                'email' => 'required|email|unique:users-web',
                'phone' => 'required|numeric|unique:users-web',
            ]);


            $insert =  new UserWeb();
            $insert->name = $request->input('name' );
            $insert->email = $request->input('email' );
            $insert->password = md5($request->input('email' ));
            $insert->phone = $request->input('phone' );
            $insert->serialNo = $request->input('serialNo' );
            $insert->groupId = $request->input('groupId' );
            $insert->addUser = 2;
            //
            $insert->levelNo = 0;
            $insert->parentId = 0;
            $insert->mainRoot = 0;
            $insert->addBy = 0;
            $insert->isActive = 1;
            $insert->save();
            return redirect()->action('Admin\UserWebController@index');
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function edit($id)
    {
        //
        if (Auth::user()) {
            $allData = UserWeb::all();
            $allGroup = Usergroups::all();
            $editData = UserWeb::find($id);
            //Tree
            $checkData = UserWeb::where('parentId' , '=' , $id)->get();
            $editData->check = count($checkData);

          //  self::display_children($id);


            return  view('admin.user.edit')->with(['editData' => $editData , 'allData' => $allData , 'allGroup' => $allGroup]);
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()) {
            $this->validate($request, [
                'name' => 'required',

            ]);
            //
            if(empty($request->input('password' )))
            {
                $pass = $request->input('oldPassword' );
            }
            else{
                $pass = $request->input('password' );
            }

            $update =  UserWeb::find($id);
            $update->name = $request->input('name' );
            $update->email = $request->input('email' );
            $update->password = md5($pass);
            $update->phone = $request->input('phone' );
            $update->serialNo = $request->input('serialNo' );
            $update->levelNo = $request->input('levelNo' );
            $update->parentId = $request->input('parentId' );
            $update->groupId = $request->input('groupId' );
            $update->addUser = $request->input('addUser' );
            $update->addBy = 0;
            $update->isActive = 1;
            $update->save();

            return redirect()->action('Admin\UserWebController@index');
        } else {
            return redirect()->route('login');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


    public function adminActive(Request $request)
    {
        $userId = $request->userId ;
        $isActive = $request->isActive ;

        DB::table('users-web')
            ->where('id', $userId)
            ->update(['isActive' => $isActive]);

        echo $isActive;
    }


    public function addUser($id)
    {
        //
        if (Auth::user()) {
            $allData = UserWeb::all();
            $serialNo = count($allData) + 1 ;
            $allGroup = Usergroups::all();

            $userData = UserWeb::find($id);
            if($userData->mainRoot != 0)
            {
                $mainRoot = UserWeb::find($userData->mainRoot);
            }
            else{
                $mainRoot = UserWeb::find($id);
            }

            //Tree
            $treeData = UserWeb::where('parentId' , '=' , $id)->get();

            return view('admin.user.create')->with([ 'serialNo' => $serialNo , 'allData' => $allData ,'allGroup' => $allGroup ,
                'userName' => $mainRoot->name , 'mainRoot' => $mainRoot->id , 'userData' => $userData ,
                'treeData' => $treeData ,]);
        } else {
            return redirect()->route('login');
        }


    }




    function display_children( $parent) {

        $sidebaroutputString = '';
        $mydata = UserWeb::display_children( $parent);
        if (count($mydata) > 0) {
            foreach ($mydata as $mytreedata) {

                $parentch = UserWeb::display_children( $mytreedata->id);
                $sidebaroutputString .= ' <li>'. $mytreedata->name . '> ';

                if (count($parentch) > 0) {
                    $sidebaroutputString .= ' <span class="arrow "></span>';
                }
                $sidebaroutputString .= '</a>';

                if (count($parentch) > 0) {

                    $sidebaroutputString .= ' <ul class="sub-menu">';
                    display_children($mytreedata->id);

                    $sidebaroutputString .= ' </ul>';
                }

                $sidebaroutputString .= ' </li>';
            }
        }
        return $sidebaroutputString;
    }

}
