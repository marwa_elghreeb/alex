<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Message;
use App\MessageDetails;
use App\UserWeb;
use App\User;

class MessageUsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = Message::all();
        return view('admin.messageUser.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'message' => 'required',
            'recipientId' => 'required',
        ]);

        //checkData
        $checkData = Message::where
        ([ 'senderId' => 1, 'recipientId' => $request->input('recipientId')  ])
            ->orWhere([ 'senderId' => $request->input('recipientId') , 'recipientId' => 1  ])->get();

        if(count($checkData) > 0)
        {
            //Message Details
            $details = new MessageDetails();
            $details->messageId = $checkData[0]->id;
            $details->userId = 1;
            $details->message = $request->input('message');
            $details->save();

        }
        else{
            //Insert
            $insert = new Message();
            $insert->senderId = 1;
            $insert->senderType = 'admin';
            $insert->recipientId = $request->input('recipientId');
            $insert->recipientType = 'user';
            $insert->save();
            //Message Details
            $details = new MessageDetails();
            $details->messageId = $insert->id;
            $details->userId = 1;
            $details->message = $request->input('message');
            $details->save();
        }



        return redirect()->action('Admin\MessageUsersController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Message Details
        $details = MessageDetails::where('messageId' , $id)->get();
        return view('admin.messageUser.details')->with([ 'details' => $details ]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if($id == 1)
        {
            $allUser = User::all();
        }
        elseif ($id == 2)
        {
            $allUser = UserWeb::all();
        }
        return view('admin.messageUser.send')->with([ 'allUser' => $allUser ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
