<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use App\Language;
use Auth;
use App\Sections;

class SectionsControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Sections::all();
        foreach ($category as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];
        }
        return view('admin.section.index')->with('category', $category);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        $all = Sections::all();
        foreach ($all as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];
        }
        return view('admin.section.create')->with([ 'allLang'=> $allLang ,'all'=> $all ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $allLang = Language::all();
        foreach ($allLang as $data) {
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
            $tittles[$data->symbol] = $request->input('tittle_' . $data->symbol);
            $contents[$data->symbol] = $request->input('content_' . $data->symbol);
        }
        $nameArr = json_encode($names);
        $tittleArr = json_encode($tittles);
        $contentsArr = json_encode($contents);

        //Insert
        $main = new Sections();
        $main->parentId = $request->input('parentId' );
        $main->name = $nameArr;
        $main->tittle = $tittleArr;
        $main->content = $contentsArr;
        $main->save();
       // dd($main);
        return redirect()->action('Admin\SectionsControllers@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $main = Sections::find($id);
        $nameArr = json_decode($main->name , true);
        $tittleArr = json_decode($main->tittle , true);
        $contentsArr = json_decode($main->content , true);
        $all = Sections::all();
        foreach ($all as $data)
        {
            $nameArr = json_decode($data->name , true);
            $data->name = $nameArr[Lang::getLocale()];
        }

        return view('admin.section.edit')->with(['main'=> $main ,'all'=> $all ,
            'nameArr' => $nameArr , 'tittleArr' => $tittleArr  , 'contentsArr' => $contentsArr , 'allLang' => $allLang]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
            $tittles[$data->symbol] = $request->input('tittle_' . $data->symbol);
            $contents[$data->symbol] = $request->input('content_' . $data->symbol);
        }
        $nameArr = json_encode($names);
        $tittleArr = json_encode($tittles);
        $contentsArr = json_encode($contents);
        //Insert
        $main =  Sections::find($id);
        $main->parentId = $request->input('parentId' );
        $main->name = $nameArr;
        $main->tittle = $tittleArr;
        $main->content = $contentsArr;
        $main->save();
        //
        return redirect()->action('Admin\SectionsControllers@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sections::where('id' , $id)->delete();
        return redirect()->action('Admin\SectionsControllers@index');
    }
}
