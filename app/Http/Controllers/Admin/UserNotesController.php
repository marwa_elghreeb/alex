<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\User;
use App\UserNotes;

class UserNotesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = User::all();

        return view('admin.userNotes.index')->with([ 'allData' => $allData]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $userData = User::find($id);
        $userNotes = UserNotes::where('userId' , $id)->orderBy('id' , 'DESC')->get();
        return view('admin.userNotes.show')->with([ 'userData' => $userData , 'userNotes' => $userNotes]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $itr2 = $request->input('note');
     //   dd($itr2);
        for ($x = 0; $x < count($itr2); $x++) {

            if (!empty($request->input('id')[$x])) {

                $update  = UserNotes::find($request->input('id')[$x]);
                $update->userId = $id;
                $date = $request->input('noteDate')[$x];
                $update->noteDate = date('Y-m-d', strtotime($date));
                $update->note = $request->input('note')[$x];
                $update->save();
            }
            elseif (empty($request->input('id')[$x])) {

                $insert = new UserNotes();
                $insert->userId = $id;
                $date = $request->input('noteDate')[$x];
                $insert->noteDate =date('Y-m-d', strtotime($date));
                $insert->note = $request->input('note')[$x];
                $insert->save();

            }
        }
        return redirect()->action('Admin\UserNotesController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
