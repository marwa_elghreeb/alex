<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Language;
use App\Items;
use App\ItemAdd;
use App\ItemRequest;
use App\UserWeb;

class ItemRequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = ItemRequest::all();
        return view('admin.itemsRequest.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        // $editData->id = $id;
        $allItems = Items::all();
        foreach ($allItems as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        $allUserWeb = UserWeb::all();
        return view('admin.itemsRequest.create')->with([ 'allLang' => $allLang,
            'allItems' => $allItems,    'allUserWeb' => $allUserWeb,]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'quantity' => 'required|numeric',
        ]);

        //Insert
        $insert = new ItemRequest();
        $insert->itemId = $request->input('itemId');
        $insert->userId = $request->input('userId');
        $insert->quantity = $request->input('quantity');
        $insert->save();
        //update quantity
        $itemData = Items::find($request->input('itemId'));
        $itemData->quantity = ($itemData->quantity) -  $request->input('quantity');
        $itemData->save();


        return redirect()->action('Admin\ItemRequestController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $allUserWeb = UserWeb::all();
        $editData  = ItemRequest::find($id);
        $allItems = Items::all();
        foreach ($allItems as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }

        return view('admin.itemsRequest.edit')->with(['editData'=> $editData ,  'allLang' => $allLang ,
              'allUserWeb' => $allUserWeb , 'allItems' => $allItems ,  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
