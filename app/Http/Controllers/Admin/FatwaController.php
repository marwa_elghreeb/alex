<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Fatwa;
use App\FatwaReplay;

class FatwaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = Fatwa::all();
        return view('admin.fatwa.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.fatwa.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'question' => 'required',
            'replay' => 'required',
        ]);

        $insert =  new Fatwa();
        $insert->userId = 0;
        $insert->inHome = $request->input('inHome' );
        $insert->question = $request->input('question' );
        $insert->state = 0;
        $insert->save();
        //Replay
        $replay = new FatwaReplay();
        $replay->fatwaId = $insert->id;
        $replay->userId = 0;
        $replay->replay = $request->input('replay' );
        $replay->sound = '';
        $replay->state = 0;
        $replay->save();

        return redirect()->action('Admin\FatwaController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editData = Fatwa::find($id);
        $details = FatwaReplay::where('fatwaId' ,$id )->get();
        return  view('admin.fatwa.edit')->with([ 'editData' => $editData , 'details' => $details ,]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'question' => 'required',
            'replay' => 'required',
        ]);

        $insert =   Fatwa::find($id);
        $insert->userId = 0;
        $insert->inHome = $request->input('inHome' );
        $insert->question = $request->input('question' );
        $insert->state = 0;
        $insert->save();
        //Replay
       /* $replay = new FatwaReplay();
        $replay->fatwaId = $insert->id;
        $replay->userId = 0;
        $replay->replay = $request->input('replay' );
        $replay->sound = '';
        $replay->state = 0;
        $replay->save();*/

        return redirect()->action('Admin\FatwaController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Fatwa::where('id' ,$id )->delete();
        FatwaReplay::where('fatwaId' ,$id )->delete();
        return redirect()->action('Admin\FatwaController@index');
    }
}
