<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\Language;
use App\Cateory;
use App\CompetitionsLevel;
use App\Exams;
use App\ExamsQuestion;

class ExamsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allData = Exams::all();
        foreach ($allData as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.exam.index')->with('allData', $allData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $allLang = Language::all();
        $allType = Cateory::all();
        foreach ($allType as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }
        return view('admin.exam.create')->with(['allLang' => $allLang, 'allType' => $allType]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //Insert
        $insert = new Exams();
        $insert->name = json_encode($names);;
        $insert->competitionsId = $request->input('competitionsId');
        $insert->levelId = $request->input('levelId');
        $insert->save();
        //insert Awards
        $count = $request->input('name');
        for ($x = 0; $x < count($count); $x++) {
            $subItem = new ExamsQuestion();
            $subItem->examId = $insert->id;
            $subItem->name = $request->input('name')[$x];
            $subItem->save();
        }
        return redirect()->action('Admin\ExamsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allLang = Language::all();
        $editData = Exams::find($id);
        $nameArr = json_decode($editData->name, true);
        $editData->name = $nameArr[Lang::getLocale()];

        $allType = Cateory::all();
        foreach ($allType as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }

        $allLevel = CompetitionsLevel::where('typeId' , $editData->competitionsId )->get();
        foreach ($allLevel as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];

        }

        $subData = ExamsQuestion::where('examId' , $id)->get();

        return view('admin.exam.edit')->with(['editData' => $editData, 'allLang' => $allLang,
            'allType' => $allType , 'allLevel' => $allLevel , 'subData' => $subData]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $allLang = Language::all();
        foreach ($allLang as $data) {
            $this->validate($request, [
                'name_' . $data->symbol => 'required',
            ]);
            $names[$data->symbol] = $request->input('name_' . $data->symbol);
        }
        //Insert
        $insert =  Exams::find($id);
        $insert->name = json_encode($names);;
        $insert->competitionsId = $request->input('competitionsId');
        $insert->levelId = $request->input('levelId');
        $insert->save();
        //insert Sub
        ExamsQuestion::where('examId', $id)->delete();
        $count = $request->input('name');
        for ($x = 0; $x < count($count); $x++) {
            $subItem = new ExamsQuestion();
            $subItem->examId = $insert->id;
            $subItem->name = $request->input('name')[$x];
            $subItem->save();
        }
        return redirect()->action('Admin\ExamsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Exams::where('id', $id)->delete();
        ExamsQuestion::where('examId', $id)->delete();
        return redirect()->action('Admin\ExamsController@index');
    }
}
