<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Competitors;

class CompetitorsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $allData = Competitors::all();
        return view('admin.competitors.index')->with([
            'allData' => $allData,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.competitors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'userName' => 'required|unique:competitors',
            'nationalId' => 'required|numeric|min:14',
            'phone1' => 'required|numeric',
            'phone2' => 'numeric',
            'email' => 'required|unique:competitors|email',
            'age' => 'numeric',
        ]);

        if ($request->hasFile('imageName')) {
            //
            $imageName = time() . '.' . $request->file('imageName')->getClientOriginalExtension();
            $request->file('imageName')->move(public_path('images'), $imageName);
        } else {
            $imageName = '';
        }
        $data =  new Competitors();
        $data->imageName = $imageName;
        $data->userName = $request->input('userName' );
        $data->nationalId = $request->input('nationalId' );
        $data->city = $request->input('city' );
        $data->area = $request->input('area' );
        $data->address = $request->input('address' );
        $data->phone1 = $request->input('phone1' );
        $data->phone2 = $request->input('phone2' );
        $data->phoneHome = $request->input('phoneHome' );
        $data->email = $request->input('email' );
        $data->education = $request->input('education' );
        $data->job = $request->input('job' );
        $data->gender = $request->input('gender' );
        $data->age = $request->input('age' );
        $data->shareBefore = $request->input('shareBefore' );
        $data->save();
        return redirect()->action('Admin\CompetitorsController@index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $editData = Competitors::where('userId' ,$id)->first();
        $editData->id = $id;
        return  view('admin.competitors.edit')->with(['editData'=> $editData ,  ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'userName' => 'required',
            'nationalId' => 'required|numeric|min:14',
            'phone1' => 'required|numeric',
            'phone2' => 'numeric',
            'email' => 'required|email',
            'age' => 'numeric',
        ]);

        if ($request->hasFile('imageName')) {
            //
            $imageName = time() . '.' . $request->file('imageName')->getClientOriginalExtension();
            $request->file('imageName')->move(public_path('images'), $imageName);
        } else {
            $imageName = $request->input('oldImage' );
        }
        $data =   Competitors::find($id);
        $data->code = $id;
        $data->imageName = $imageName;
        $data->userName = $request->input('userName' );
        $data->nationalId = $request->input('nationalId' );
        $data->city = $request->input('city' );
        $data->area = $request->input('area' );
        $data->address = $request->input('address' );
        $data->phone1 = $request->input('phone1' );
        $data->phone2 = $request->input('phone2' );
        $data->phoneHome = $request->input('phoneHome' );
        $data->email = $request->input('email' );
        $data->education = $request->input('education' );
        $data->job = $request->input('job' );
        $data->gender = $request->input('gender' );
        $data->age = $request->input('age' );
        $data->shareBefore = $request->input('shareBefore' );
        $data->save();
        return redirect()->action('Admin\CompetitorsController@index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
