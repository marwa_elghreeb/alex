<?php

namespace App\Http\Controllers\Admin;


use App\Competitions;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Lang;
use Auth;
use App\User;
use App\UserCompetitions;
use App\Competitors;
use App\CompetitionsLevel;
use App\competitorsQuranMemorization;
use App\Cateory;
use App\Notifications;

class UserCompetitionsControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $catData = Cateory::find($id);
        $nameArr = json_decode($catData->name , true);
        $catName = $nameArr[Lang::getLocale()];
        //
        $sub = Competitions::where(['catId' => $id , 'active' => 0  ])->first();
        $nameArr1 = json_decode($sub->name , true);
        $subName = $nameArr1[Lang::getLocale()];

        //
        $allData = UserCompetitions::where([  'mainCompetitionsId' => $id  ])->get();
        foreach ($allData as $data)
        {
            $level = $data->Level->name;
            $nameArr1 = json_decode($level , true);
            $data->levelName = $nameArr1[Lang::getLocale()];

        }

        return view('admin.userCompetitors.show')->with([ 'allData' =>  $allData , 'id' => $id
        , 'catName' => $catName  , 'subName' => $subName , ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'city' => 'required',
        ]);
        //update
        $update =   competitorsQuranMemorization::where('userId' , $id)->first();
        $update->userId = $id;
        $update->teatcherId = $request->input('teatcherId' );
        $update->code = $request->input('code' );
        $update->jobCode = $request->input('jobCode' );
        $update->job = $request->input('job' );
        $update->relatedTo = $request->input('relatedTo' );
        $update->level = $request->input('level' );
        $update->readType = $request->input('readType' );
        if ($request->hasFile('images')) {
            $files = $request->file('images');
            $imgArr = array();
            foreach ($files as $file) {
                $filename = uniqid() . '_' . $file->getClientOriginalName();
                $file->move(public_path('/images/'),$filename);
                $imgArr[] = $filename;
                $img = implode(',' ,$imgArr);
            }
        } else {
            $img = '';
        }

        $update->images =  $img;
        $update->save();
        return redirect()->action('Admin\UserCompetitionsControllers@show' , $id);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }


    public function addComp(Request $request)
    {
        $userId = $request->userId;
        $compId = $request->compId;

        $chek = UserCompetitions::where(['mainCompetitionsId' => $compId , 'userId' => $userId ])->get();
        if(count($chek) > 0)
        {
            UserCompetitions::where('id' , $chek[0]->id )->delete();
            echo -1;
        }
        else{
            $insert =  new UserCompetitions();
            $insert->userId = $userId;
            $insert->competitionsId = $compId;
            $insert->active = 0;
            $insert->save();
            echo 1;

        }


    }


    public function completeUserData($id)
    {
        //
        $editData = Competitors::find($id);
        $all = User::where('groupId' , 2)->get();
        $level = CompetitionsLevel::where('typeId' , 3)->get();
        foreach ($level as $data) {
            $nameArr = json_decode($data->name, true);
            $data->name = $nameArr[Lang::getLocale()];
        }
        $details = competitorsQuranMemorization::where('userId' , $id)->first();

      // dd($details);
        return view('admin.userCompetitors.complete')->with([ 'editData'=> $editData  ,'all'=> $all,
            'level'=> $level,  'details'=> $details, ]);
    }


    public function activeCopm(Request $request)
    {
        UserCompetitions::where('id' , $request->id)->update([
            'active' => $request->isActive
        ]);
        $checkData = UserCompetitions::find($request->id);

        //Send notify to user
        $notify = new Notifications();
        $notify->userId = $checkData->userId;
        $notify->fromUserId = 'الاداره';
        if ($request->isActive == 1)
        {
            $notify->message = 'تم مراجعه البيانات ... وسيتم تحديد موعد للاختبار فى اقرب وقت';

        }else{
            $notify->message = 'تم رفض الطلب لعدم استكمال البيانات';
        }
        $notify->state = 0;
        $notify->save();
    }
}
