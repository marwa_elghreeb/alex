<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserWeb extends Model
{
    //
    protected $table =  'users-web';

    public function UserDetails()
    {
        return $this->belongsTo('App\Competitors' ,'userId' ,'id');
    }
}
