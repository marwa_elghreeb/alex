<?php

namespace App\Http\PermissionTraits;

use App\Models\Role;
use App\Models\Permission;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PermissionsModel
{

    protected function checkPermissions($action, $user, $permissions, $controllername, $type = 'array', $role = false)
    {


        if ($permissions->count() == 0) {
            return false;
        }

        if ($type == 'object' && isset($permissions->id)) {
            $permissions = [$permissions];
        }
        $array = [];
        // $action = $this->returnArray($action);
        $controllername = $this->returnArray($controllername);


        foreach ($permissions as $permission) {
            if (in_array($permission->controller_name, $controllername)) {

                if ($role !== false) {
                    $array[$permission->controller_name]['actions'] = $this->checkAction($permission, $action, $user, $controllername);
                } else {
                    $array['actions'] = $this->checkAction($permission, $action, $user, $controllername);
                }
            } else {
                $array[$permission->controller_name] = false;
            }
        }


        return count($array) == 0 ? false : $array;

    }

    protected function checkAction($permission, $actions, $user, $controllername)
    {

        $response = [];
        $permissionprop = Permission::where(['controller_name' => $controllername])
            ->whereRaw('FIND_IN_SET("' . $actions . '",name)')->first();

        if ($permissionprop->count() > 0) {

            $grouppermission = $this->getGroupPermissions($user, $permissionprop->id);
            if ($grouppermission->count() > 0)
                $response[$actions] = true;
            else
                $response[$actions] = false;
        } else {
            $response[$actions] = false;
        }
        return count($response) == 0 ? false : $response;


    }


    protected function getGroupPermissions($user, $response)
    {

        $permissions = Role::where('id', $user->role_id)->with(['permission' => function ($query) use ($response) {
            return $query->whereIn('permissions.id', $this->returnArray($response))->orderBy('id', 'desc');
        }])->first();

        return $permissions->permission;
    }


    protected function returnArray($array)
    {
        return is_array($array) ? $array : [$array];
    }


    protected function checkIfControllerExist($controllername)
    {
        return Permission::where('controller_name', $this->returnArray($controllername))->first();
    }


    public function can($user, $action, $controllername)
    {
        $checkGroupPermission = $this->checkPermissions($action, $user, $this->checkIfControllerExist($controllername), $controllername, 'object', true);
        return $checkGroupPermission;
    }

    public function canUser($user, $action, $controllername)
    {

        $per = $this->can($user, $action, $controllername);
        return isset($per[$controllername]['actions'][$action]) ? $per[$controllername]['actions'][$action] : false;
    }
}