<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExamesDate extends Model
{
    //
    public function Competitions() {
        return $this->belongsTo('App\Cateory','competitionsId','id') ;
    }

    public function Level() {
        return $this->belongsTo('App\CompetitionsLevel','levelId','id') ;
    }
}
