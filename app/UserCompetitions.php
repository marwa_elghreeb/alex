<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCompetitions extends Model
{
    public function User()
    {
        return $this->belongsTo('App\UserWeb' ,'userId' ,'id');
    }

    public function UserDetails()
    {
        return $this->belongsTo('App\Competitors' ,'userId' ,'id');
    }

    public function Compitetion()
    {
        return $this->belongsTo('App\Competitions' ,'competitionsId' ,'id');
    }


    public function Level()
    {
        return $this->belongsTo('App\CompetitionsLevel' ,'levelId' ,'id');
    }
}
