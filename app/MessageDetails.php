<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MessageDetails extends Model
{
    public function SenderData() {
        return $this->belongsTo('App\UserWeb','senderId','id') ;
    }

    public function RecipientData() {
        return $this->belongsTo('App\UserWeb','recipientId','id') ;
    }
}
