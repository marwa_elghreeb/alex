<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityQuarter extends Model
{
    public function Government() {
        return $this->belongsTo('App\City','cityId','id') ;
    }

    public function City() {
        return $this->belongsTo('App\CityState','stateId','id') ;
    }
}
