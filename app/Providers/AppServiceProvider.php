<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Setting;
use App\Social;
use App\Pages;
use App\Cateory;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //DB
        Schema::defaultStringLength(191);

        //Site Data
        $setting = Setting::find(1);
        $social = Social::all();
        $category = Cateory::take(2)->get();

       // $headerPages = Pages::where('location' , '=' , 1)->get();
      //  $footerPage = Pages::where('location' , '=' , 2)->get();
        //
        view()->share([
            'setting' => $setting,
            'social' => $social,
            'category' => $category,
          //  'headerPages' => $headerPages,
          //  'footerPage' => $footerPage,
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }


}
