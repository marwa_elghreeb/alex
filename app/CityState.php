<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CityState extends Model
{
    public function Government() {
        return $this->belongsTo('App\City','cityId','id') ;
    }
}
