$(document).ready(function (e) {
   //  alert('in js');
    /*================================*/

    $('.nested input[type=checkbox]').click(function () {
        //   alert(66);
        $(this).parent().find('li input[type=checkbox]').prop('checked', $(this).is(':checked'));
        var sibs = false;
        $(this).closest('ul').children('li').each(function () {
            if ($('input[type=checkbox]', this).is(':checked')) sibs = true;
        })
        $(this).parents('ul').prev().prop('checked', sibs);
    });

    /*================================*/


    /*===Image Itr===================================*/
    $('#addImage').click(function () {
        $("#imageContiner").append('<div class="fileinput fileinput-new" data-provides="fileinput">'
            + ' <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 200px; height: 150px;"> </div>'
            + '  <div>'
            + '  <span class="btn red btn-outline btn-file">'
            + '  <span class="fileinput-new"> Select image </span>'
            + '<span class="fileinput-exists"> Change </span>'
            + '  <input type="file" name="imageName[]"> </span>'
            + '  <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>'
            + '<a class="btn default removeItem"> <i class="fa fa-times"></i> </a>'
            + '  </div>'
            + '  </div> <br/>');
    });

    $(document).on('click', '.removeItem', function () {
        $(this).parent().parent().remove();
    });

    $(document).on('click', '.removeItemEdit', function () {

        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        var tableName = $(this).attr('data-tableName');
        // alert(url);
        $.post(url, {tableName: tableName, id: id, _token: CSRF_TOKEN}, function (data) {
            location.reload();
        });
        $(this).parent().parent().remove();
    });


    /*================================*/

    $('#addSocial').click(function () {
        // alert(5555);
        var socialCount = $('#socialCount').val();
        socialCount++;
        $("#productContiner").append('<tr>'
            + '<td> <input type="text" class="form-control" name="name[]" ></td>'
            + ' <td> <input type="text" class="form-control " name="icon[]"  ></td>'
            + ' <td> <input type="text" class="form-control" name="link[]" ></td>'
            + '<td><a class="btn default removeItem"> <i class="fa fa-times"></i> </a></td> </tr>');

        $('#socialCount').val(socialCount);
        socialCount++;

        $('.iconClass').iconpicker();
    });
    /*====================================*/
    $('.adminActive').click(function () {

        var isActive = $(this).attr('data-isActive');
        var userId = $(this).attr('data-userId');
        var url = $('#url').val();
        //alert(url);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.post(url, {isActive: isActive, userId: userId, _token: CSRF_TOKEN}, function (data) {
            // alert(data);
            if (data > 0) {
                location.reload();
            }
        });
    });

    /*================================*/
    $('#addItem').click(function () {
       // alert(5555);
        var itemCount = $('#itemCount').val();
        itemCount++;
        $("#itemContiner").append('<tr>'
            + '<td> <input type="text" class="form-control" name="itemName[]" ></td>'
            + '<td><a class="btn default removeItem"> <i class="fa fa-times"></i> </a></td> </tr>');

        $('#itemCount').val(itemCount);
        itemCount++;
    });


    /*=Competition===============================*/

    $(".getLevel").change(function () {
        //alert($(this).val());

        var id = $(this).val();
        var url = $(this).attr('data-url');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.post(url, {id: id, _token: CSRF_TOKEN}, function (data) {
            // alert(data);
            $('#levelData').html(data);
        });
    });


    /*=Quantity===============================*/

    $("#quantity").keypress(function () {
        var mainQuantity = $('#mainQuantity').val();
        var quantity = $(this).val();
         newQuantity = Number(mainQuantity) - Number(quantity);

        $('#newQuantity').val(newQuantity);

    });

    $(".getQuantity").change(function () {
       // alert($(this).val());
        var id = $(this).val();
        var url = $(this).attr('data-url');
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.post(url, {id: id, _token: CSRF_TOKEN}, function (data) {
           // alert(data);
            $('#newQuantity').val(data);
        });

    });


    /*=SendMessage===============================*/
    $('#sendMessage').click(function () {
        alert(5555);

    });



    /****City *****************************/
    $('.getCity').change(function () {
        var cityId = $(this).val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var url = $(this).attr('data-url');
        // alert(CSRF_TOKEN);
        $.post(url, {cityId: cityId, _token: CSRF_TOKEN}, function (data) {

            // alert(data);
            $('#stateId').html(data);

        });
    });


    $('.getSubCity').on("change", function () {
        var cityId = $(this).val();
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        var url = $(this).attr('data-url');
        $.post(url, {cityId: cityId, _token: CSRF_TOKEN}, function (data) {
            $('#stateId1').html(data);
        });
    });


    /*=AddComp===============================*/
    $('.addComp').click(function () {
        var compId = $(this).val();
        var userId = $(this).attr('data-userId');
        var url = $(this).attr('data-url');
        //alert(url);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.post(url, {compId: compId, userId: userId, _token: CSRF_TOKEN}, function (data) {
            // alert(data);

           /*  if(data == 1)
             {
                 setTimeout(function(){
                     alert("Hello");

                     }, 3000);
             }
             else {
                 setTimeout(function(){ alert("Hello");

                 }, 3000);
             }*/
        });
    });

    /*===============================*/
    function isNumberKey(evt)
    {
        var charCode = (evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;
    }

    /*=============================*/
    /*================================*/
    $('#addNotes').click(function () {

        var itemCount = $('#itemCount').val();
        itemCount++;
        $("#notesContainer").append('<tr>'

           +' <td><input type="date" class="form-control" name="noteDate[]"/></td>'
            +' <td> <textarea class="form-control" rows="6" name="note[]"></textarea></td>'

            + '<td><a class="btn default removeItem"> <i class="fa fa-times"></i> </a></td> </tr>');

        $('#itemCount').val(itemCount);
        itemCount++;
    });


    /*====================================*/
    $('.activeComp').click(function () {

       // alert(333);

        var isActive = $(this).attr('data-isActive');
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-url');
        //alert(url);
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $.post(url, {isActive: isActive, id: id, _token: CSRF_TOKEN}, function (data) {
            // alert(data);
           // if (data > 0) {
                location.reload();
            //}
        });
    });






});